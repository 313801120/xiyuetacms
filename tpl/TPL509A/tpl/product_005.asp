﻿<% 
'#禁止自动更新当前文件'  #号去掉代表此文件不被程序自动更新替换掉

                 '''
'               (0 0)
'   +-----oOO----(_)------------+
'   |                           |
'   |    author:XIYUETA         |
'   |    QQ:313801120           |
'   |    www.xiyueta.com        |
'   |                           |
'   +------------------oOO------+
'              |__|__|
'               || ||
'              ooO Ooo

cssName="product005" 
 %>
<style>


.product-fluid<%=cssName%> article,.product-fluid<%=cssName%> aside,.product-fluid<%=cssName%> blockquote,.product-fluid<%=cssName%> body,.product-fluid<%=cssName%> button,.product-fluid<%=cssName%> code,.product-fluid<%=cssName%> dd,.product-fluid<%=cssName%> details,.product-fluid<%=cssName%> div,.product-fluid<%=cssName%> dl,.product-fluid<%=cssName%> dt,.product-fluid<%=cssName%> fieldset,.product-fluid<%=cssName%> figcaption,.product-fluid<%=cssName%> figure,.product-fluid<%=cssName%> footer,.product-fluid<%=cssName%> form,.product-fluid<%=cssName%> h1,.product-fluid<%=cssName%> h2,.product-fluid<%=cssName%> h3,.product-fluid<%=cssName%> h4,.product-fluid<%=cssName%> h5,.product-fluid<%=cssName%> h6,.product-fluid<%=cssName%> header,.product-fluid<%=cssName%> hgroup,.product-fluid<%=cssName%> hr,.product-fluid<%=cssName%> input,.product-fluid<%=cssName%> legend,.product-fluid<%=cssName%> li,.product-fluid<%=cssName%> menu,.product-fluid<%=cssName%> nav,.product-fluid<%=cssName%> ol,.product-fluid<%=cssName%> p,.product-fluid<%=cssName%> pre,.product-fluid<%=cssName%> section,.product-fluid<%=cssName%> td,.product-fluid<%=cssName%> textarea,.product-fluid<%=cssName%> th,.product-fluid<%=cssName%> ul{
    margin: 0;
    padding: 0;
}

.product-fluid<%=cssName%> *{
    outline: 0;
    -webkit-text-size-adjust: none;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}
.product-fluid<%=cssName%> *{
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    
}
 
.product-fluid<%=cssName%> .clearfix:after{
    content: ".";
    display: block;
    height: 0;
    clear: both;
    overflow: hidden;
    visibility: hidden;
}
.product-fluid<%=cssName%> .clearfix{
    zoom: 1
}
.product-fluid<%=cssName%> ul,.product-fluid<%=cssName%> li{
    list-style: none;
}
.product-fluid<%=cssName%> a{
    color: #333;
    text-decoration: none;
}
.product-fluid<%=cssName%> a:hover{
    text-decoration: none;
}
.product-fluid<%=cssName%> a:focus,.product-fluid<%=cssName%> a:hover,.product-fluid<%=cssName%> a:visited,.product-fluid<%=cssName%> input,.product-fluid<%=cssName%> input:hover,.product-fluid<%=cssName%> input:focus,.product-fluid<%=cssName%> input:active,.product-fluid<%=cssName%> select{
  text-decoration: none;
  outline: none !important;
  border-radius: 0px;
  -webkit-border-radius: 0px;
  /*去掉iOS系统input标签默认样式*/
  -webkit-appearance: none;
   }
.product-fluid<%=cssName%> .left{
    float:left;
}
.product-fluid<%=cssName%> .right{
    float:right;
}
.product-fluid<%=cssName%> img{
    border:0;
    display: block;
    overflow: hidden;
    max-width: 100%;
    height:auto;
}
 
.product-fluid<%=cssName%> .container{
    width: 1200px;
    margin: 0px auto;
}
.product-fluid<%=cssName%> {
    padding: 30px 0px;
    padding-top: 20px;
}
.product-fluid<%=cssName%> .product-sidebar{
    float: left;
    width: 280px;
}
.product-fluid<%=cssName%> .product-sidebar .title{
    background: #0d62be;
    width: 100%;
    text-align: center;
    line-height: 70px;
    color: #fff;
    font-size: 26px;
    font-weight: 600;
    letter-spacing: 1px;
}
.product-fluid<%=cssName%> .product-sidebar .list ul li a{
    display: block;
    width: 100%;
    font-size: 15px;
    padding: 5px 15px;
    line-height: 35px;
    border: 1px solid #ddd;
    border-top: 0
}
.product-fluid<%=cssName%> .product-sidebar .list ul li a:hover{
    background:  #edefff;
    padding-left: 20px;
}
.product-fluid<%=cssName%> .product-sidebar .list ul li a i.iconfont{
    margin-right: 5px;
}
.product-fluid<%=cssName%> .product-right{
    float: right;
    width: calc(100% - 280px);
    padding-left: 30px;
}
.product-fluid<%=cssName%> .product-right .main-title{
    display: none;
}
.product-fluid<%=cssName%> .product-r-title{
    border-bottom: 1px solid #ddd;
    margin-bottom: 10px;
    line-height: 35px;
}
.product-fluid<%=cssName%> .product-r-title .left .title span{
    margin-right: 10px;
    color: #0d62be;
    font-size: 24px;
    font-weight: 600;
}
.product-fluid<%=cssName%> .product-r-title .left .title em{
    color: #999;
    text-transform: uppercase;
}
.product-fluid<%=cssName%> .product-r-title .right a{
    font-size: 14px;
    color: #666;
}
.product-fluid<%=cssName%> .product-r-title .right a:hover{
    color: #0d62be;
}
.product-fluid<%=cssName%> .product-item{
    float: left;
    width: 25%;
    padding: 10px;
}
.product-fluid<%=cssName%> .product-item a{
    display: block;
    width: 100%;
    border: 1px solid #ddd;
}
.product-fluid<%=cssName%> .product-item a .product-img{
    width: 100%;
    overflow: hidden;
}
.product-fluid<%=cssName%> .product-item a .product-img img{
    width: 100%;
}
.product-fluid<%=cssName%> .product-info{
    width: 100%;
    text-align: center;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    font-size: 14px;
    color: #666;
    line-height: 35px;
    height: 35px;
}
.product-fluid<%=cssName%> .product-item a:hover .product-info{
    color: #0d62be;
}
.product-fluid<%=cssName%> .product-more{
    display: none;
}
.product-fluid<%=cssName%> .main-title{
    text-align: center;
    margin-bottom: 20px;
}
.product-fluid<%=cssName%> .main-title .title{
    font-size: 30px;
    color: #333;
    line-height: 45px;
}
.product-fluid<%=cssName%> .main-title .english{
    text-transform: uppercase;
    font-size: 15px;
    margin-top: 5px;
    color: #666;
}
.product-fluid<%=cssName%> .fwzx-box{
    background: #0d62be;
    padding: 20px 0px;
    text-align: center;
    color: #fff;
}
.product-fluid<%=cssName%> .fwzx-box .fwzx-tel{
    font-size: 30px;
    margin-bottom: 10px;
    font-weight: bold;
    text-shadow: 0px 0px 3px #666;
}
.product-fluid<%=cssName%> .fwzx-more{
    width: 120px;
    border-radius: 10px;
    box-shadow: 0px 0px 3px #ddd;
    display: block;
    margin: 10px auto 0px;
    text-align: center;
    background: #fff;
    line-height: 35px;
}
@media (max-width:640px ){
.product-fluid<%=cssName%> .container{
        width:96%;
    }
.product-fluid<%=cssName%> section{
        padding:30px 0px;
    }
.product-fluid<%=cssName%> .product-sidebar{
        width:100%;
        display: none;
    }
.product-fluid<%=cssName%> .product-right{
        width:100%;
        padding-left:0px;
    }
.product-fluid<%=cssName%> .product-right .main-title{
        display: block;
    }
.product-fluid<%=cssName%> .product-r-title{
        display: none;
    }
.product-fluid<%=cssName%> .product-item{
        width:50%;
    }
.product-fluid<%=cssName%> .product-more{
        display: block;
    }
.product-fluid<%=cssName%> .product-more a{
        display: block;
        width:120px;
        line-height:40px;
        text-align: center;
        margin:20px auto 0px;
        background: #0d62be;
        color:#fff;
        border-radius: 5px;
        overflow: hidden;

    }
.product-fluid<%=cssName%> .main-title .title{
        font-size:24px;
        font-weight: 600;
        line-height: 1.5;
    }
.product-fluid<%=cssName%> .main-title .english{
        font-size:14px;
    }}
</style>

    <!-- header -->
    <section class="product-fluid<%=cssName%>">
        <div class="container clearfix">
            <div class="product-sidebar">
                <div class="title">产品分类</div>
                <div class="list">
                    <ul>
<%rs.open "select * from " & db_PREFIX & "webcolumn where  isthrough=1 AND parentid in("& getNameToAllId( "产品中心") &") order by sortRank desc,id desc" ,conn,1,1
for i=1 to 12
if rs.eof then exit for
%>
                        <li>
                            <a href="<%=getNavUrl(rs("id"),rs("columnType"))%>">
                                <i class="iconfont icon-jiantou_xiangyouliangci"></i><%=uTitle & rs("columnName")%>
                            </a>
                        </li> 
<%rs.movenext:next:rs.close%>
                    </ul>
                </div>
                <!-- 服务咨询电话 -->
                <div class="fwzx-box">
                    <p>服务咨询热线</p>
                    <p class="fwzx-tel"><%=webphone%></p>
                    <a href="tencent://message/?uin=<%=webqq%>&Site=xiyueta.com&Menu=yes" class="fwzx-more" target="_blank">在线咨询</a>
                </div>
            </div>
            <div class="product-right">
                <div class="product-r-title clearfix">
                    <div class="left">
                        <div class="title">
                            <span>产品中心</span>
                            <em>product center</em>
                        </div>
                    </div>
                    <div class="right">
                        <a href="<%=getNavNameToUrl("","产品中心")%>">查看更多+</a>
                    </div>
                </div>
                <div class="main-title">
                    <div class="title">产品中心</div>
                    <div class="english">product center</div>
                </div>
                <div class="product-list clearfix">




<%rs.open "select * from " & db_PREFIX & "ArticleDetail where isthrough=1 AND parentId in("& getNameToAllId( "产品中心") &") order by sortRank asc,id desc" ,conn,1,1
for i=1 to 12
if rs.eof then exit for
%> 
                    <div class="product-item">
                        <a href="<%=getArticleUrl(rs("id"))%>">
                            <div class="product-img"><img src="<%=rs("smallimage")%>" style="height: 140.7px;">
                            </div>
                            <div class="product-info"><%=uTitle&rs("title")%></div>
                        </a>
                    </div>

<%rs.movenext:next:rs.close%>

                </div>
                <div class="product-more">
                    <a href="<%=getNavNameToUrl("","产品中心")%>">查看更多</a>
                </div>
            </div>
        </div>
    </section>
    <!-- 选择我们的理由 -->
