﻿<% 
'#禁止自动更新当前文件'  #号去掉代表此文件不被程序自动更新替换掉

                 '''
'               (0 0)
'   +-----oOO----(_)------------+
'   |                           |
'   |    author:XIYUETA         |
'   |    QQ:313801120           |
'   |    www.xiyueta.com        |
'   |                           |
'   +------------------oOO------+
'              |__|__|
'               || ||
'              ooO Ooo

cssName="page-home001" 
 %>
<style>

.container<%=cssName%> html{-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;line-height:1.15;-webkit-tap-highlight-color: rgba(0,0,0,0);}
.container<%=cssName%> body{margin:0;font-family: "Microsoft Yahei",PingFangSC-Regular,"Helvetica Neue",Helvetica,Arial,"Hiragino Sans GB","Heiti SC","WenQuanYi Micro Hei",sans-serif;line-height: 1.42857143;font-size: 14px;min-width: 1230px;background: #fff;color: #333;}
.container<%=cssName%> a{-webkit-text-decoration-skip:objects;background-color:transparent;}
.container<%=cssName%> small{font-size:80%;}
.container<%=cssName%> img{border-style:none;}
.container<%=cssName%> body,.container<%=cssName%> button,.container<%=cssName%> input,.container<%=cssName%> select,.container<%=cssName%> textarea{text-rendering: optimizeLegibility;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;-moz-font-feature-settings: "liga","kern";}
.container<%=cssName%> ::-webkit-file-upload-button{-webkit-appearance:button;font:inherit;}
.clearfix<%=cssName%>{zoom:1;}
.clearfix<%=cssName%>:before,.clearfix<%=cssName%>:after{display:table;line-height:0;content:"";}
.clearfix<%=cssName%>:after{clear:both;}
.container<%=cssName%> a{color: #333;text-decoration: none;}
.container<%=cssName%> a:hover{color: #0d62be;text-decoration: none;}
.container<%=cssName%> ul{margin: 0;list-style: none;padding: 0;}
.container<%=cssName%> .img-center{text-align: center;font-size: 0;}
.container<%=cssName%> .img-center img{display: inline-block;width: auto;height: auto;max-width: 100%;max-height: 100%;vertical-align: middle;}
.container<%=cssName%> .img-cover{overflow: hidden;}
.container<%=cssName%> .img-cover span{display: block;width: 100%;background: no-repeat center / cover;-webkit-transition: all 0.5s;transition: all 0.5s;}
.container<%=cssName%>{padding: 0 10px;width: 1200px;margin: 0 auto;overflow-x: hidden;}
.container<%=cssName%> .yunu-link{padding: 20px 0;}
.container<%=cssName%> .yunu-link h4{border-bottom: 1px solid #ddd;margin: 0;font-size: 18px;color: #0d62be;padding-bottom: 15px;}
.container<%=cssName%> .yunu-link h4 small{text-transform: uppercase;font-size: 16px;font-weight: normal;color: #999;}
.container<%=cssName%> .yunu-link ul{margin-left: -20px;}
.container<%=cssName%> .yunu-link li{float: left;margin-left: 20px;margin-top: 20px;}
.container<%=cssName%> .ct2-sd{float: left;width: 228px;}
.container<%=cssName%> .ct2-mn{float: right;width: 940px;}
.container<%=cssName%> .panel-sd{margin-bottom: 15px;border: 1px solid #eee;}
.container<%=cssName%> .panel-sd .tit{text-align: center;color: #fff;line-height: 50px;font-size: 18px;background: -webkit-linear-gradient(bottom, #0d62be, #005a8c);
    background: -moz-linear-gradient(bottom, #0d62be, #005a8c);
    background: -o-linear-gradient(bottom, #0d62be, #005a8c);
    background: linear-gradient(bottom, #0d62be, #005a8c);}
.container<%=cssName%> .lanmu li{border-bottom: 1px dotted #ddd;}
.container<%=cssName%> .lanmu li:last-child{border-bottom: 0;}
.container<%=cssName%> .lanmu li>a{display: block;background: -webkit-linear-gradient(right, #fafafa, #f0f0f0);
    background: -moz-linear-gradient(right, #fafafa, #f0f0f0);
    background: -o-linear-gradient(right, #fafafa, #f0f0f0);
    background: linear-gradient(right, #fafafa, #f0f0f0);line-height: 44px;padding: 0 0 0 25px;}
.container<%=cssName%> .lanmu li>a:hover,.container<%=cssName%> .lanmu li.active>a{background: #326ec0;color: #fff;}
.container<%=cssName%> .list-3{padding: 0 15px 15px;}
.container<%=cssName%> .list-3 li a{display: block;margin: 15px 0 0;background-color: #f5f5f5;transition: all 0.5s;}
.container<%=cssName%> .list-3 li .img-center{overflow: hidden;}
.container<%=cssName%> .list-3 li .img-center img{transition: all 0.5s;}
.container<%=cssName%> .list-3 li .text{text-align: center;padding: 0 10px;line-height: 30px;height: 30px;overflow: hidden;font-size: 12px;}
.container<%=cssName%> .list-3 li a:hover{background-color: #0d62be;color: #fff;}
.container<%=cssName%> .list-3 li a:hover .img-center img{transform: scale(1.8);}
.container<%=cssName%> .contact-sd{padding: 15px;}
.container<%=cssName%> .contact-sd h4{margin: 0;color: #0d62be;}
.container<%=cssName%> .contact-sd p{font-size: 12px;border-top: 1px dotted #ddd;margin: 8px 0 0;padding-top: 8px;}
.container<%=cssName%> .position{margin-bottom: 15px;border: 1px solid #efefef;border-left: 3px solid #0d62be;padding: 8px 0 8px 10px;background: #fafafa;color: #0d62be;font-weight: bold;}
.container<%=cssName%> .mn-box{border: 0px solid #efefef;padding: 20px;margin-bottom: 15px;}
.container<%=cssName%> .panel-mn{border: 1px solid #efefef;margin-bottom: 15px;border-top: 2px solid #0d62be;}
.container<%=cssName%> .panel-mn .tit{padding: 0 20px;background: -webkit-linear-gradient(bottom, #f5f5f5, #eee);
    background: -moz-linear-gradient(bottom, #f5f5f5, #eee);
    background: -o-linear-gradient(bottom, #f5f5f5, #eee);
    background: linear-gradient(bottom, #f5f5f5, #eee);line-height: 44px;font-weight: bold;color: #0d62be;}
.container<%=cssName%> .panel-mn .bd{padding: 20px;}
.container<%=cssName%> .list-5{margin-left: -30px;}
.container<%=cssName%> .list-5 li{float: left;width: 50%;}
.container<%=cssName%> .list-5 li a{display: block;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;font-size: 12px;position: relative;padding-left: 10px;line-height: 30px;margin-left: 30px;}
.container<%=cssName%> .list-5 li a:before{content: '';position: absolute;left: 0;top: 50%;width: 0;height: 0;margin-top: -3px;border-top: 3px solid transparent;border-left: 4px solid #0d62be;border-bottom: 3px solid transparent;}
.container<%=cssName%> .list-5 li span{float: right;color: #999;}
.container<%=cssName%> .list-6{margin: -20px 0 0 -20px;}
.container<%=cssName%> .list-6 li{float: left;width: 25%;}
.container<%=cssName%> .list-6 li a{display: block;margin: 20px 0 0 20px;}
.container<%=cssName%> .list-6 li .img-cover{border: 2px solid #0d62be;transition: all 0.5s;}
.container<%=cssName%> .list-6 li .img-cover span{padding-top: 74%;}
.container<%=cssName%> .list-6 li .text{margin-top: 10px;text-align: center;}
.container<%=cssName%> .list-6 li .text h4{font-size: 14px;margin: 0;font-weight: normal;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;}
.container<%=cssName%> .list-6 li .text p{margin: 5px 0 0;font-size: 12px;color: #888;height: 34px;overflow: hidden;}
.container<%=cssName%> .list-6 li a:hover .img-cover{border-color: #0d62be;}
.container<%=cssName%> .list-6 li a:hover .img-cover span{transform: scale(1.8);}
@media screen and (max-width: 767px){
.container<%=cssName%> body{min-width: 320px;padding-top: 50px;}
.container<%=cssName%>{width: auto;}
.container<%=cssName%> .ct2-sd{display: none;}
.container<%=cssName%> .ct2-mn{float: none;width: auto;}
.container<%=cssName%> .position{font-size: 12px;margin: 0 -15px 20px;border-right: 0;padding-right: 15px;}
.container<%=cssName%> .mn-box{padding: 0;border: none;}
.container<%=cssName%> .panel-mn .tit{padding: 0 10px;}
.container<%=cssName%> .panel-mn .bd{padding: 10px;}
.container<%=cssName%> .list-5 li{width: 100%;}
.container<%=cssName%> .list-6{margin: -10px 0 0 -10px;}
.container<%=cssName%> .list-6 li{width: 50%;}
.container<%=cssName%> .list-6 li a{margin: 10px 0 0 10px;}}

</style>

    <!--/搜索和热门关键词-->
    <div class="container<%=cssName%> clearfix<%=cssName%>">
        <!--左侧边栏-->
        <div class="ct2-sd">
            <!--产品分类-->
            <div class="panel-sd">
                <div class="tit">产品中心</div>
                <ul class="lanmu"> 
<%rs.open "select * from " & db_PREFIX & "webcolumn where  isthrough=1 AND parentid in("& getNameToAllId( "产品中心") &") order by sortRank asc,id asc" ,conn,1,1
for i=1 to 12
if rs.eof then exit for
%>
<li><a href="<%=getNavUrl(rs("id"),rs("columnType"))%>"><%=uTitle & rs("columnName")%></a></li>
<%rs.movenext:next:rs.close%>
                </ul>
            </div>
            <!--热门产品-->
            <div class="panel-sd">
                <div class="tit">热门产品</div>
                <ul class="list-3">
                   <%
rs.open "select * from " & db_PREFIX & "ArticleDetail where isthrough=1 AND parentId in("& getNameToAllId("产品中心") &")order by views desc" ,conn,1,1
for i=1 to 5
if rs.eof then exit for
%>   
                    <li>
                        <a href="<%=getArticleUrl(rs("id"))%>">
                            <div class="img-center"><img src="<%=rs("smallimage")%>">
                            </div>
                            <div class="text"><%=rs("title")%></div>
                        </a>
                    </li>
    <%rs.movenext:next:rs.close%>
                </ul>
            </div>
            <!--联系我们-->
            <div class="panel-sd">
                <div class="tit">联系我们</div>
                <div class="contact-sd">
                    <h4><%=webcompany%></h4>
                    <p>地址：<%=webaddress%></p>
                    <p>手机：<%=webphone%></p>
                    <p>电话：<%=webtel%></p>
                    <p>邮箱：<%=webemail%></p>
                </div>
            </div>
        </div>
        <div class="ct2-mn">
            <!--产品列表-->
            <div class="position">产品中心</div>
            <div class="bd">
                <ul class="list-6 clearfix<%=cssName%>">


<%rs.open "select * from " & db_PREFIX & "ArticleDetail where isthrough=1 AND parentId in("& getNameToAllId( "产品中心") &") order by sortRank asc,id asc" ,conn,1,1
for i=1 to 8
if rs.eof then exit for
%>
 <li>
                        <a href="<%=getArticleUrl(rs("id"))%>">
                            <div class="img-cover">
                                <span style="background-image: url(<%=rs("smallimage")%>);"></span>
                            </div>
                            <div class="text">
                                <h4><%=uTitle&rs("title")%></h4>
                                <p><%=cutStr(delhtml(rs("bodyContent")),30,"...")%></p>
                            </div>
                        </a>
                    </li>
<%rs.movenext:next:rs.close%>


                   
                </ul>
            </div>
            <!--/产品列表-->
            <div class="mn-box">
                <ul class="list-4"></ul>
            </div>
            <!--新闻列表-->
            <div class="panel-mn">
                <div class="tit">新闻中心</div>
                <div class="bd">
                    <ul class="list-5 clearfix<%=cssName%>">
                       <%
rs.open "select * from " & db_PREFIX & "ArticleDetail where isthrough=1 AND parentId in("& getNameToAllId("新闻资讯") &")order by views desc" ,conn,1,1
for i=1 to 10
if rs.eof then exit for
%>  
                        <li>
                            <a href="<%=getArticleUrl(rs("id"))%>">
                                <span><%=format_Time(rs("createTime"),30)%></span><%=rs("title")%>
                            </a>
                        </li>
    <%rs.movenext:next:rs.close%>
                    </ul>
                </div>
            </div>
            <!--/新闻列表-->
            <!--案例列表-->
            <div class="panel-mn">
                <div class="tit">案例列表</div>
                <div class="bd">
                    <ul class="list-6 clearfix<%=cssName%>">
                       <%
rs.open "select * from " & db_PREFIX & "ArticleDetail where isthrough=1 AND parentId in("& getNameToAllId("产品中心") &")order by views desc" ,conn,1,1
for i=1 to 8
if rs.eof then exit for
%>  
                        <li> 
                            <a href="<%=getArticleUrl(rs("id"))%>">
                                <div class="img-cover">
                                    <span style="background-image: url(<%=rs("smallimage")%>);"></span>
                                </div>
                                <div class="text">
                                    <h4><%=rs("title")%></h4>
                                </div>
                            </a>
                        </li>
    <%rs.movenext:next:rs.close%>
                    </ul>
                </div>
            </div>
            <!--/案例列表-->
            <!--友情链接-->
            <div class="yunu-link" data-aos="fade-up">
                <h4>友情链接
                    <small>/ LINKS</small>
                </h4>
                <ul class="clearfix<%=cssName%>">
       

<%rs.open "select * from " & db_PREFIX & "friendlink where isthrough=1 order by sortrank asc" ,conn,1,1
for i=1 to 30
if rs.eof then exit for
%>
<li><a href="<%=rs("httpurl")%>"<%=IIF(rs("nofollow")=1," rel=""nofollow""","")%><%=IIF(rs("target")<>""," target="""& rs("target") &"""","")%>><%=rs("title")%></a></li>
<%rs.movenext:next:rs.close%>


                </ul>
            </div>
        </div>
    </div>
