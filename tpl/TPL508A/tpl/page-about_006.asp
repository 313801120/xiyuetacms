﻿<% 
'#禁止自动更新当前文件'  #号去掉代表此文件不被程序自动更新替换掉

                 '''
'               (0 0)
'   +-----oOO----(_)------------+
'   |                           |
'   |    author:XIYUETA         |
'   |    QQ:313801120           |
'   |    www.xiyueta.com        |
'   |                           |
'   +------------------oOO------+
'              |__|__|
'               || ||
'              ooO Ooo

resurl="/web/tpl/page-about/006/"
 %>
<style>
.pageabout006 body{
    overflow-x: hidden;
    font-family: "Microsoft YaHei";
}
.pageabout006 ul{
    padding: 0;
    margin: 0;
}
.pageabout006 ul li{
    list-style: none;
}
.pageabout006 a{
    color: #337ab7;
    text-decoration: none;
}
.pageabout006 h1,.pageabout006 h2,.pageabout006 h3,.pageabout006 h4,.pageabout006 h5{
    margin: 0;
}
.pageabout006 a:focus,.pageabout006 a:hover{
    color: #23527c;
    text-decoration: none;
}
.pageabout006 a:focus{
    outline: none;
    outline-offset: -2px;
}
.pageabout006 .hot-keys:after,.pageabout006 .xypg-download-list .download-title:after,.pageabout006 .xypg-download-list .download-item:after,.pageabout006 .xypg-download-list .download-item .download-item-span:after,.pageabout006 .xypg-job-list .job-title:after,.pageabout006 .xypg-job-list .job-item:after,.pageabout006 .xypg-job-list .job-item .job-item-span:after,.pageabout006 .xypg-detail-info-bar:after,.pageabout006 .xypg-detail-info-bar .xypg-file-down .file-down-list li:after,.pageabout006 .xypg-detail-pn div:after,.pageabout006 .xypg-detail-file-dwon:after,.pageabout006 .product-detail-tab .product-detail-tabli:after,.pageabout006 .tag-list-product .tag-wz .tab-fenli:after{
    content: "";
    display: table;
    clear: both;
}
.pageabout006 .x-header-right .x-login .username,.pageabout006 .x-product-list li h3,.pageabout006 .x-case-item .wz h3,.pageabout006 .x-news-top h3,.pageabout006 .x-news-list li a,.pageabout006 .latest-news li a,.pageabout006 .hot-keys li a,.pageabout006 .xypg-left-nav>li>a,.pageabout006 .xypg-left-nav .xypg-left-subnav>li>a,.pageabout006 .xypg-left-nav .xypg-left-threenav>li>a,.pageabout006 .xypg-product-list li h3 a,.pageabout006 .xypg-case-list li h3 a,.pageabout006 .xypg-news-list li .tit h3,.pageabout006 .xypg-album-cate li .album-title,.pageabout006 .xypg-album-list li h3,.pageabout006 .xypg-download-list .download-item .download-item-span.download-item-first,.pageabout006 .xypg-download-list .download-item .download-item-span .download-item-con,.pageabout006 .xypg-job-list .job-item .job-item-span .job-item-con,.pageabout006 .xypg-detail-info-bar .xypg-file-down .file-down-list li .list-con p,.pageabout006 .xypg-detail-pn div a,.pageabout006 .relate-product-slick .owl-item p,.pageabout006 .relate-news-list li a,.pageabout006 .xypg-jod-detail .job-detail-title,.pageabout006 .cover-item .item-first .wz h3,.pageabout006 .cover-item .item-list h4,.pageabout006 .tag-list-product .tag-wz h1,.pageabout006 .tag-list-product .tag-wz .tab-fenli p{
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
}
.pageabout006 .x-banner .owl-carousel .banner-item a,.pageabout006 .x-product-list li .img,.pageabout006 .x-case-item .img,.pageabout006 .x-news-img,.pageabout006 .page-banner,.pageabout006 .xypg-product-list li .img,.pageabout006 .xypg-case-list li .img,.pageabout006 .relate-product-slick .owl-item .img,.pageabout006 .x-logo a,.pageabout006 .x-layout-logo,.pageabout006 .x-layout-ewm img,.pageabout006 .page-message-img{
    font-size: 0;
}
.pageabout006 .x-logo a img,.pageabout006 .x-layout-logo img,.pageabout006 .x-layout-ewm img img,.pageabout006 .page-message-img img{
    max-width: 100%;
}
@media (max-width: 768px){
.pageabout006 body{
        padding-top: 60px;
    }}
.pageabout006 .icon-font{
    position: relative;
}
.pageabout006 .icon-font:after{
    content: "";
    width: 30px;
    height: 30px;
    background-image: url(<%=resUrl%>images/icon_spirit.png);
    background-repeat: no-repeat;
    position: absolute;
    left: calc(50% - 15px);
    top: calc(50% - 15px);
}
.pageabout006 .page-position{
    padding: 15px 0;
    font-size: 14px;
    color: #2f2f2f;
}
.pageabout006 .page-position a{
    color: #2f2f2f;
}
.pageabout006 .page-position a:hover{
    color: #0d62be;
}
.pageabout006 .page-wrap{
    background: #ffffff;
    padding-bottom: 3.38541667%;
}
@media (min-width: 990px){
.pageabout006 .page-wrap-left{
        float: left;
        width: 305px;
        margin-right:30px;
    }}
.pageabout006 .xypg-left-box{
    margin-bottom: 20px;
}
.pageabout006 .xypg-left-title{
    background: #0d62be;
    padding: 10px 20px;
    position: relative;
}
.pageabout006 .xypg-left-title h3{
    font-size: 20px;
    color: #ffffff;
    line-height: 30px;
    font-weight: bold;
    position: relative;
    z-index: 9;
}
.pageabout006 .xypg-left-title i{
    position: absolute;
    left: -5px;
    top: -9px;
    font-style: normal;
    font-size: 67px;
    font-weight: bold;
    opacity: .1;
    color: #fff;
    line-height: 1;
    z-index: 1;
    text-transform: uppercase;
    display: none;
}
.pageabout006 .xypg-left-title span{
    font-weight: normal;
    font-size: 16px;
    color: #fff;
    margin-left: 10px;
    text-transform: capitalize;
}
.pageabout006 .xypg-left-title span:before{
    content: "/";
    padding-right: 5px;
}
.pageabout006 .xypg-left-con{
    border: 1px solid #d2d2d2;
    border-top: none;
}
.pageabout006 .latest-news{
    padding: 10px 0;
}
.pageabout006 .latest-news li{
    padding: .5em 1em;
    font-size: 14px;
}
.pageabout006 .latest-news li a{
    display: block;
    line-height: 1.2;
    color: #5a5a5a;
}
.pageabout006 .latest-news li a:before{
    content: '+';
    font-weight: bold;
    margin-right: 5px;
    font-size: 16px;
}
.pageabout006 .latest-news li a:hover{
    color: #0d62be;
}
.pageabout006 .latest-news li a:hover:before{
    color: #0d62be;
}
.pageabout006 .hot-keys{
    padding: 20px 7px 8px 7px;
}
.pageabout006 .hot-keys li{
    width: calc(50% - 14px);
    float: left;
    margin: 0 7px 14px 7px;
}
.pageabout006 .hot-keys li a{
    display: block;
    text-align: center;
    font-size: 14px;
    color: #5a5a5a;
    line-height: 2.857;
    border: 1px dashed #f1f1f1;
}
.pageabout006 .hot-keys li a:hover{
    color: #0d62be;
    border-color: #0d62be;
}
.pageabout006 .page-wrap-contact{
    padding: 10px 0;
    font-size: 14px;
}
.pageabout006 .page-wrap-contact h4{
    padding: .3em 1.4em .8em 1.4em;
    font-size: 16px;
    color: #000000;
}
.pageabout006 .page-wrap-contact p{
    border-top: 1px solid #eee;
    position: relative;
    margin-bottom: 0;
    padding: .8em 1.4em;
    color: #2f2f2f;
}
.pageabout006 .page-message-img img{
    border: 1px solid #d2d2d2;
}
.pageabout006 .page-wrap-right{
    overflow: hidden;
}
.pageabout006 .page-mob-tool{
    position: fixed;
    right: 15px;
    bottom: 100px;
    z-index: 9999;
}
.pageabout006 .page-mob-tool li{
    width: 40px;
    height: 40px;
    line-height: 40px;
    text-align: center;
    margin-bottom: 1px;
    cursor: pointer;
    position: relative;
}
.pageabout006 .page-mob-tool li:before{
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;
    background: #0d62be;
    opacity: .7;
}
.pageabout006 .page-mob-tool li i{
    display: block;
    width: 100%;
    height: 100%;
}
.pageabout006 .page-mob-tool li .icon-dots-horizontal:after{
    background-position: -30px -390px;
}
.pageabout006 .page-mob-tool li .icon-top:after{
    background-position: -30px -60px;
}
@media (min-width: 992px){
.pageabout006 .page-mob-tool{
        display: none;
    }}
.pageabout006 .xymob-left-close-btn{
    display: none;
}
@media (max-width: 990px){
.pageabout006 body.no-scroll,.pageabout006 html.no-scroll{
        height: 100vh;
        overflow: hidden;
    }
.pageabout006 .xymob-menu-click{
        position: fixed;
        width: 100vw;
        overflow-y: scroll;
        top: 0;
        left: 100%;
        -webkit-transition: all .5s ease;
        transition: all .5s ease;
        z-index: 999999;
        height: 100vh;
        background: rgba(0, 0, 0, 0.7);
    }
.pageabout006 .xymob-menu-click .xypg-left-news,.pageabout006 .xymob-menu-click .xypg-left-keys,.pageabout006 .xymob-menu-click .xypg-left-contact,.pageabout006 .xymob-menu-click .page-message-img{
        display: none;
    }
.pageabout006 .xymob-menu-click .xymob-left-close-btn{
        display: block;
        position: absolute;
        top: 15px;
        right: 15px;
        width: 32px;
        height: 32px;
        line-height: 32px;
        border-radius: 50%;
        border: 1px solid #fff;
        text-align: center;
        background: black;
    }
.pageabout006 .xymob-menu-click .xymob-left-close-btn .icon-font{
        display: block;
        width: 100%;
        height: 100%;
    }
.pageabout006 .xymob-menu-click .xymob-left-close-btn .icon-font:after{
        background-position: -30px -300px;
    }
.pageabout006 .xymob-menu-click .xypg-left{
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        min-height: 90vh;
        padding: 5vh 0;
    }
.pageabout006 .xymob-menu-click .xypg-left .xypg-left-menu{
        width: 90%;
        background: #fff;
    }
.pageabout006 .xymob-menu-click.click{
        left: 0;
    }}
.pageabout006 .xypg-left-nav{
    margin: 0 -1px;
}
.pageabout006 .xypg-left-nav>li{
    position: relative;
    border-bottom: 1px solid #eeeeee;
}
.pageabout006 .xypg-left-nav>li:last-child{
    border-bottom: none;
}
.pageabout006 .xypg-left-nav>li>a{
    display: block;
    font-size: 16px;
    line-height: 22px;
    color: #2e2e2e;
    padding: 15px 18px;
}
.pageabout006 .xypg-left-nav>li .first-nav-btn{
    position: absolute;
    z-index: 99;
    width: 30px;
    height: 30px;
    text-align: center;
    top: 10px;
    right: 20px;
    cursor: pointer;
    background: url(images/icon_spirit.png) 0 -240px no-repeat;
}
.pageabout006 .xypg-left-nav>li:hover>a,.pageabout006 .xypg-left-nav>li.clicked>a{
    color: #0d62be;
}
.pageabout006 .xypg-left-nav>li:hover .first-nav-btn,.pageabout006 .xypg-left-nav>li.clicked .first-nav-btn{
    color: #0d62be;
}
.pageabout006 img{
    image-rendering: -moz-crisp-edges; /* Firefox */
    image-rendering: -o-crisp-edges; /* Opera */
    image-rendering: -webkit-optimize-contrast; /* Webkit (non-standard naming) */
    image-rendering: crisp-edges;
    -ms-interpolation-mode: nearest-neighbor; /* IE (non-standard property) */
}
.pageabout006 .tong{
  max-width: 1200px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 15px; }
.pageabout006 p{
  margin: 0; }


</style>

<span class="pageabout006">
    <div class="page-position">
        <div class="tong clearfix">您当前的位置 ：<%=navLocation(navid,"")%>
        </div>
    </div>
    <div class="page-wrap">
        <div class="tong clearfix">
            <div class="page-wrap-left xymob-menu-click">
                <div class="xymob-left-close-btn">
                    <i class="icon-font icon-close"></i>
                </div>
                <div class="xypg-left">
                    <div class="xypg-left-box xypg-left-menu">
                        <div class="xypg-left-title">
                            <h3><%=nav%>
                                <span><%=ennav%></span>
                            </h3>
                            <i>a</i>
                        </div>
                        <div class="xypg-left-con">
                            <ul class="xypg-left-nav">


 

 <%  
            if navid<>"" or parentid<>"" then
                if parentid=-1 then 
                    addsql=" and parentid=" & navid
                else
                    addsql=" and parentid=" & getRootNavId(parentid)
                end if
            end if
            rs.open "select * from " & db_PREFIX & "webcolumn where isthrough=1 "& addsql &" order by sortRank asc" ,conn,1,1
            if not rs.eof then
                while not rs.eof
            %>

                    <li class="<%=IIF(rs("columnname")=columnname,"clicked","")%>">
                        <a href="<%=getNavUrl(rs("id"),rs("columntype"))%>"><%=uTitle & rs("columnname")%></a>
                                    <div class="first-nav-btn"></div>
                    </li>

                    <%rs.movenext:wend
            else
            %>
                    <li class="clicked">
                        <a href="<%=getNavUrl(navid,columntype)%>"><%=uTitle & columnname%></a>
                                    <div class="first-nav-btn"></div>
                    </li>

            <%end if:rs.close%>



                       


                            </ul>
                        </div>
                    </div>
                    <div class="xypg-left-box xypg-left-news">
                        <div class="xypg-left-title">
                            <h3>新闻资讯
                                <span>News</span>
                            </h3>
                            <i>N</i>
                        </div>
                        <div class="xypg-left-con">
                            <ul class="latest-news">


<%
rs.open "select * from " & db_PREFIX & "ArticleDetail where isthrough=1 AND parentId in("& getNameToAllId("新闻资讯") &")order by views desc" ,conn,1,1
for i=1 to 6
if rs.eof then exit for
%>  
                        <li>
                            <a href="<%=getArticleUrl(rs("id"))%>"><%=uTitle & rs("title")%></a>
                        </li>
    <%rs.movenext:next:rs.close%>
 
                            </ul>
                        </div>
                    </div>
                        <%
                    '不需要
                    if 1=2 then%>
                    <div class="xypg-left-box xypg-left-keys">
                        <div class="xypg-left-title">
                            <h3>热门关键词
                                <span>Keywords</span>
                            </h3>
                            <i>K</i>
                        </div>
                        <div class="xypg-left-con">
                            <ul class="hot-keys">
                                <li>
                                    <a href="1111">111</a>
                                </li>                                 
                            </ul>
                        </div>
                    </div>
                    <%end if%>
                    <div class="xypg-left-box xypg-left-contact">
                        <div class="xypg-left-title">
                            <h3>联系我们
                                <span>Contact Us</span>
                            </h3>
                            <i>C</i>
                        </div>
                        <div class="xypg-left-con">
                            <div class="page-wrap-contact">
                                <h4><%=webcompany%></h4>
                                <p>电 话：<%=webtel%></p>
                                <p>传 真：<%=webfax%></p>
                                <p>邮 箱：<%=webemail%></p>
                                <p>地 址：<%=webaddress%></p>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="page-wrap-right">
                <div class="xypg-right-content">
                    <p style="line-height:28px;">
                        <%=bodyContent%>
                    </p>
                </div>
            </div>
            <div class="page-mob-tool">
                <ul>
                    <li class="xymob-page-navbtn">
                        <i class="icon-font icon-dots-horizontal"></i>
                    </li>
                    <li class="xymob-page-backtop" onclick="$('html,body').animate({scrollTop: '0px'}, 500);">
                        <i class="icon-font icon-top"></i>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<script>
$(function(){

    // 内页手机端端 做的导航弹出特效
    $(".xymob-page-navbtn").click(function(){
        $(".xymob-menu-click").addClass('click');
        $('html,body').addClass('no-scroll');
    });
    $(".xymob-left-close-btn").click(function(){
        $(".xymob-menu-click").removeClass('click');
        $('html,body').removeClass('no-scroll');
    });
})    
</script>
</span>
