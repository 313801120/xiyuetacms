﻿<% 
'#禁止自动更新当前文件'  #号去掉代表此文件不被程序自动更新替换掉

                 '''
'               (0 0)
'   +-----oOO----(_)------------+
'   |                           |
'   |    author:XIYUETA         |
'   |    QQ:313801120           |
'   |    www.xiyueta.com        |
'   |                           |
'   +------------------oOO------+
'              |__|__|
'               || ||
'              ooO Ooo

resurl="/web/tpl/page-detail/006/"
 %>
<style>

.pagedetail006 body{
    overflow-x: hidden;
    font-family: "Microsoft YaHei";
}
.pagedetail006 ul{
    padding: 0;
    margin: 0;
}
.pagedetail006 ul li{
    list-style: none;
}
.pagedetail006 a{
    color: #337ab7;
    text-decoration: none;
}
.pagedetail006 h1,.pagedetail006 h2,.pagedetail006 h3,.pagedetail006 h4,.pagedetail006 h5{
    margin: 0;
}
.pagedetail006 a:focus,.pagedetail006 a:hover{
    color: #23527c;
    text-decoration: none;
}
.pagedetail006 a:focus{
    outline: none;
    outline-offset: -2px;
}
.pagedetail006 .hot-keys:after,.pagedetail006 .xypg-download-list .download-title:after,.pagedetail006 .xypg-download-list .download-item:after,.pagedetail006 .xypg-download-list .download-item .download-item-span:after,.pagedetail006 .xypg-job-list .job-title:after,.pagedetail006 .xypg-job-list .job-item:after,.pagedetail006 .xypg-job-list .job-item .job-item-span:after,.pagedetail006 .xypg-detail-info-bar:after,.pagedetail006 .xypg-detail-info-bar .xypg-file-down .file-down-list li:after,.pagedetail006 .xypg-detail-pn div:after,.pagedetail006 .xypg-detail-file-dwon:after,.pagedetail006 .product-detail-tab .product-detail-tabli:after,.pagedetail006 .tag-list-product .tag-wz .tab-fenli:after{
    content: "";
    display: table;
    clear: both;
}
.pagedetail006 .x-header-right .x-login .username,.pagedetail006 .x-product-list li h3,.pagedetail006 .x-case-item .wz h3,.pagedetail006 .x-news-top h3,.pagedetail006 .x-news-list li a,.pagedetail006 .latest-news li a,.pagedetail006 .hot-keys li a,.pagedetail006 .xypg-left-nav>li>a,.pagedetail006 .xypg-left-nav .xypg-left-subnav>li>a,.pagedetail006 .xypg-left-nav .xypg-left-threenav>li>a,.pagedetail006 .xypg-product-list li h3 a,.pagedetail006 .xypg-case-list li h3 a,.pagedetail006 .xypg-news-list li .tit h3,.pagedetail006 .xypg-album-cate li .album-title,.pagedetail006 .xypg-album-list li h3,.pagedetail006 .xypg-download-list .download-item .download-item-span.download-item-first,.pagedetail006 .xypg-download-list .download-item .download-item-span .download-item-con,.pagedetail006 .xypg-job-list .job-item .job-item-span .job-item-con,.pagedetail006 .xypg-detail-info-bar .xypg-file-down .file-down-list li .list-con p,.pagedetail006 .xypg-detail-pn div a,.pagedetail006 .relate-product-slick .owl-item p,.pagedetail006 .relate-news-list li a,.pagedetail006 .xypg-jod-detail .job-detail-title,.pagedetail006 .cover-item .item-first .wz h3,.pagedetail006 .cover-item .item-list h4,.pagedetail006 .tag-list-product .tag-wz h1,.pagedetail006 .tag-list-product .tag-wz .tab-fenli p{
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
}
.pagedetail006 .x-banner .owl-carousel .banner-item a,.pagedetail006 .x-product-list li .img,.pagedetail006 .x-case-item .img,.pagedetail006 .x-news-img,.pagedetail006 .page-banner,.pagedetail006 .xypg-product-list li .img,.pagedetail006 .xypg-case-list li .img,.pagedetail006 .relate-product-slick .owl-item .img,.pagedetail006 .x-logo a,.pagedetail006 .x-layout-logo,.pagedetail006 .x-layout-ewm img,.pagedetail006 .page-message-img{
    font-size: 0;
}
.pagedetail006 .x-banner .owl-carousel .banner-item a img,.pagedetail006 .x-product-list li .img img,.pagedetail006 .x-case-item .img img,.pagedetail006 .x-news-img img,.pagedetail006 .page-banner img,.pagedetail006 .xypg-product-list li .img img,.pagedetail006 .xypg-case-list li .img img,.pagedetail006 .relate-product-slick .owl-item .img img{
    width: 100%;
}
.pagedetail006 .x-logo a img,.pagedetail006 .x-layout-logo img,.pagedetail006 .x-layout-ewm img img,.pagedetail006 .page-message-img img{
    max-width: 100%;
}
.pagedetail006 .x-product-list li .img,.pagedetail006 .x-case-item .img,.pagedetail006 .x-news-img,.pagedetail006 .xypg-product-list li .img,.pagedetail006 .xypg-case-list li .img,.pagedetail006 .relate-product-slick .owl-item .img{
    overflow: hidden;
}
.pagedetail006 .x-product-list li .img img,.pagedetail006 .x-case-item .img img,.pagedetail006 .x-news-img img,.pagedetail006 .xypg-product-list li .img img,.pagedetail006 .xypg-case-list li .img img,.pagedetail006 .relate-product-slick .owl-item .img img{
    -webkit-transition: all .5s ease;
    transition: all .5s ease;
}
.pagedetail006 .x-product-list li .img:hover img,.pagedetail006 .x-case-item .img:hover img,.pagedetail006 .x-news-img:hover img,.pagedetail006 .xypg-product-list li .img:hover img,.pagedetail006 .xypg-case-list li .img:hover img,.pagedetail006 .relate-product-slick .owl-item .img:hover img{
    -webkit-transform: scale(1.1);
    transform: scale(1.1);
}
@media (max-width: 768px){
.pagedetail006 body{
        padding-top: 60px;
    }}
.pagedetail006 .icon-font{
    position: relative;
}
.pagedetail006 .icon-font:after{
    content: "";
    width: 30px;
    height: 30px;
    background-image: url(<%=resUrl%>images/icon_spirit.png);
    background-repeat: no-repeat;
    position: absolute;
    left: calc(50% - 15px);
    top: calc(50% - 15px);
}
.pagedetail006 .page-position{
    padding: 15px 0;
    font-size: 14px;
    color: #2f2f2f;
}
.pagedetail006 .page-position a{
    color: #2f2f2f;
}
.pagedetail006 .page-position a:hover{
    color: #0d62be;
}
.pagedetail006 .page-wrap{
    background: #ffffff;
    padding-bottom: 3.38541667%;
}
@media (min-width: 990px){
.pagedetail006 .page-wrap-left{
        float: left;
        width: 305px;
        margin-right: 30px;
    }}
.pagedetail006 .xypg-left-box{
    margin-bottom: 20px;
}
.pagedetail006 .xypg-left-title{
    background: #0d62be;
    padding: 10px 20px;
    position: relative;
}
.pagedetail006 .xypg-left-title h3{
    font-size: 20px;
    color: #ffffff;
    line-height: 30px;
    font-weight: bold;
    position: relative;
    z-index: 9;
}
.pagedetail006 .xypg-left-title i{
    position: absolute;
    left: -5px;
    top: -9px;
    font-style: normal;
    font-size: 67px;
    font-weight: bold;
    opacity: .1;
    color: #fff;
    line-height: 1;
    z-index: 1;
    text-transform: uppercase;
    display: none;
}
.pagedetail006 .xypg-left-title span{
    font-weight: normal;
    font-size: 16px;
    color: #fff;
    margin-left: 10px;
    text-transform: capitalize;
}
.pagedetail006 .xypg-left-title span:before{
    content: "/";
    padding-right: 5px;
}
.pagedetail006 .xypg-left-con{
    border: 1px solid #d2d2d2;
    border-top: none;
}
.pagedetail006 .latest-news{
    padding: 10px 0;
}
.pagedetail006 .latest-news li{
    padding: .5em 1em;
    font-size: 14px;
}
.pagedetail006 .latest-news li a{
    display: block;
    line-height: 1.2;
    color: #5a5a5a;
}
.pagedetail006 .latest-news li a:before{
    content: '+';
    font-weight: bold;
    margin-right: 5px;
    font-size: 16px;
}
.pagedetail006 .latest-news li a:hover{
    color: #0d62be;
}
.pagedetail006 .latest-news li a:hover:before{
    color: #0d62be;
}
.pagedetail006 .hot-keys{
    padding: 20px 7px 8px 7px;
}
.pagedetail006 .hot-keys li{
    width: calc(50% - 14px);
    float: left;
    margin: 0 7px 14px 7px;
}
.pagedetail006 .hot-keys li a{
    display: block;
    text-align: center;
    font-size: 14px;
    color: #5a5a5a;
    line-height: 2.857;
    border: 1px dashed #f1f1f1;
}
.pagedetail006 .hot-keys li a:hover{
    color: #0d62be;
    border-color: #0d62be;
}
.pagedetail006 .page-wrap-contact{
    padding: 10px 0;
    font-size: 14px;
}
.pagedetail006 .page-wrap-contact h4{
    padding: .3em 1.4em .8em 1.4em;
    font-size: 16px;
    color: #000000;
}
.pagedetail006 .page-wrap-contact p{
    border-top: 1px solid #eee;
    position: relative;
    margin-bottom: 0;
    padding: .8em 1.4em;
    color: #2f2f2f;
}
.pagedetail006 .page-message-img img{
    border: 1px solid #d2d2d2;
}
.pagedetail006 .page-wrap-right{
    overflow: hidden;
}
.pagedetail006 .page-mob-tool{
    position: fixed;
    right: 15px;
    bottom: 100px;
    z-index: 9999;
}
.pagedetail006 .page-mob-tool li{
    width: 40px;
    height: 40px;
    line-height: 40px;
    text-align: center;
    margin-bottom: 1px;
    cursor: pointer;
    position: relative;
}
.pagedetail006 .page-mob-tool li:before{
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;
    background: #0d62be;
    opacity: .7;
}
.pagedetail006 .page-mob-tool li i{
    display: block;
    width: 100%;
    height: 100%;
}
.pagedetail006 .page-mob-tool li .icon-dots-horizontal:after{
    background-position: -30px -390px;
}
.pagedetail006 .page-mob-tool li .icon-top:after{
    background-position: -30px -60px;
}
@media (min-width: 992px){
.pagedetail006 .page-mob-tool{
        display: none;
    }}
.pagedetail006 .xymob-left-close-btn{
    display: none;
}
@media (max-width: 990px){
.pagedetail006 body.no-scroll,.pagedetail006 html.no-scroll{
        height: 100vh;
        overflow: hidden;
    }
.pagedetail006 .xymob-menu-click{
        position: fixed;
        width: 100vw;
        overflow-y: scroll;
        top: 0;
        left: 100%;
        -webkit-transition: all .5s ease;
        transition: all .5s ease;
        z-index: 999999;
        height: 100vh;
        background: rgba(0, 0, 0, 0.7);
    }
.pagedetail006 .xymob-menu-click .xypg-left-news,.pagedetail006 .xymob-menu-click .xypg-left-keys,.pagedetail006 .xymob-menu-click .xypg-left-contact,.pagedetail006 .xymob-menu-click .page-message-img{
        display: none;
    }
.pagedetail006 .xymob-menu-click .xymob-left-close-btn{
        display: block;
        position: absolute;
        top: 15px;
        right: 15px;
        width: 32px;
        height: 32px;
        line-height: 32px;
        border-radius: 50%;
        border: 1px solid #fff;
        text-align: center;
        background: black;
    }
.pagedetail006 .xymob-menu-click .xymob-left-close-btn .icon-font{
        display: block;
        width: 100%;
        height: 100%;
    }
.pagedetail006 .xymob-menu-click .xymob-left-close-btn .icon-font:after{
        background-position: -30px -300px;
    }
.pagedetail006 .xymob-menu-click .xypg-left{
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        justify-content: center;
        min-height: 90vh;
        padding: 5vh 0;
    }
.pagedetail006 .xymob-menu-click .xypg-left .xypg-left-menu{
        width: 90%;
        background: #fff;
    }
.pagedetail006 .xymob-menu-click.click{
        left: 0;
    }}
.pagedetail006 .xypg-left-nav{
    margin: 0 -1px;
}
.pagedetail006 .xypg-left-nav>li{
    position: relative;
    border-bottom: 1px solid #eeeeee;
}
.pagedetail006 .xypg-left-nav>li:last-child{
    border-bottom: none;
}
.pagedetail006 .xypg-left-nav>li>a{
    display: block;
    font-size: 16px;
    line-height: 22px;
    color: #2e2e2e;
    padding: 15px 18px;
}
.pagedetail006 .xypg-left-nav>li .first-nav-btn{
    position: absolute;
    z-index: 99;
    width: 30px;
    height: 30px;
    text-align: center;
    top: 10px;
    right: 20px;
    cursor: pointer;
    background: url(<%=resUrl%>images/icon_spirit.png) 0 -240px no-repeat;
}
.pagedetail006 .xypg-left-nav>li:hover>a,.pagedetail006 .xypg-left-nav>li.clicked>a{
    color: #0d62be;
}
.pagedetail006 .xypg-left-nav>li:hover .first-nav-btn,.pagedetail006 .xypg-left-nav>li.clicked .first-nav-btn{
    color: #0d62be;
}
.pagedetail006 .xypg-detail-title{
    font-size: 20px;
    color: #363636;
    font-weight: bold;
    margin-bottom: 10px;
    line-height: 1.7;
}
.pagedetail006 .xypg-detail-info-bar{
    font-size: 14px;
    font-family: Arial;
    color: #9e9e9e;
    line-height: 35px;
    border-bottom: 1px dashed #cfcfcf;
    margin-bottom: 20px;
}
.pagedetail006 .xypg-detail-info-bar .detail-info-time{
    float: left;
    margin-right: 20px;
    position: relative;
    padding-left: 25px;
}
.pagedetail006 .xypg-detail-info-bar .detail-info-time i{
    position: absolute;
    width: 20px;
    height: 20px;
    top: calc(50% - 10px);
    left: 0;
}
.pagedetail006 .xypg-detail-info-bar .detail-info-time i:after{
    opacity: .5;
    background-position: 0 -360px;
}
.pagedetail006 .xypg-detail-info-bar .detail-info-numbers{
    float: left;
    position: relative;
    padding-left: 25px;
}
.pagedetail006 .xypg-detail-info-bar .detail-info-numbers i{
    position: absolute;
    width: 20px;
    height: 20px;
    top: calc(50% - 10px);
    left: 0;
}
.pagedetail006 .xypg-detail-info-bar .detail-info-numbers i:after{
    opacity: .5;
    background-position: 0 -330px;
}
.pagedetail006 .xypg-detail-con{
    font-size: 14px;
    color: #333;
    line-height: 1.7;
}
.pagedetail006 .xypg-detail-con img,.pagedetail006 .xypg-detail-con p img{
    width: auto !important;
    max-width: 100%;
    height: auto !important;
}
.pagedetail006 .xypg-detail-tags{
    margin-top: 40px;
}
.pagedetail006 .xypg-detail-tags .tags-title{
    border-bottom: 1px solid #c3c3c3;
    position: relative;
}
.pagedetail006 .xypg-detail-tags .tags-title h3{
    display: inline-block;
    padding: 0 30px;
    font-size: 16px;
    color: #0d62be;
    line-height: 40px;
    border-bottom: 3px solid #0d62be;
    margin-bottom: -1px;
}
.pagedetail006 .xypg-detail-tags .tags-title .baidu-share{
    position: absolute;
    right: 0;
    top: 8px;
}
.pagedetail006 .xypg-detail-tags .tags-content{
    padding-top: 12px;
}
.pagedetail006 .xypg-detail-tags .tags-content a{
    font-size: 12px;
    color: #2f2f2f;
    display: inline-block;
    margin-right: 9px;
    background: #f4f4f4;
    line-height: 28px;
    padding: 0 15px;
}
.pagedetail006 .xypg-detail-tags .tags-content a:hover{
    color: #0d62be;
}
.pagedetail006 .xypg-detail-url{
    margin-top: 15px;
}
.pagedetail006 .xypg-detail-url a{
    word-break: break-all;
}
.pagedetail006 .xypg-detail-pn{
    margin-top: 30px;
}
.pagedetail006 .xypg-detail-pn div+div {
    margin-top: 15px;
}
.pagedetail006 .xypg-detail-pn div{
    line-height: 22px;
    padding: 10px;
    position: relative;
    border: 1px solid #eeeeee;
}
.pagedetail006 .xypg-detail-pn div b{
    font-size: 14px;
    color: #0d62be;
    float: left;
    font-weight: normal;
}
.pagedetail006 .xypg-detail-pn div a{
    float: left;
    width: 75%;
    font-size: 14px;
    color: #666666;
}
.pagedetail006 .xypg-detail-pn div a:hover{
    color: #0d62be;
}
@media (max-width: 600px){
.pagedetail006 .xypg-detail-pn div a{
        width: 60%;
    }}
@media (max-width: 600px){
.pagedetail006 .xypg-detail-pn div a{
        width: 50%;
    }}
.pagedetail006 .xypg-detail-pn div span{
    position: absolute;
    right: 20px;
    top: 11px;
    font-size: 14px;
    color: #666666;
}
.pagedetail006 .xypg-relate{
    margin-top: 45px;
}
.pagedetail006 .relate-title{
    border: 1px solid #eeeeee;
    font-size: 14px;
    line-height: 2.857;
    margin-bottom: 15px;
}
.pagedetail006 .relate-title span{
    display: inline-block;
    color: #0d62be;
    padding: 0 1.4em;
    border-right: 1px solid #eeeeee;
}
.pagedetail006 .relate-product-slick{
    position: relative;
}
.pagedetail006 .relate-product-slick .owl-item{
    display: block;
    font-size: 0;
}
.pagedetail006 .relate-product-slick .owl-item .img{
    display: block;
    margin-bottom: 8px;
}
.pagedetail006 .relate-product-slick .owl-item p{
    margin-bottom: 0;
    padding: .5em 0;
    font-size: 12px;
    color: #0d62be;
    text-align: center;
}
.pagedetail006 .relate-product-slick .owl-item:hover p{
    color: #0d62be;
}
.pagedetail006 .relate-product-slick .owl-nav button.owl-prev,.pagedetail006 .relate-product-slick .owl-nav button.owl-next{
    width: 25px;
    height: 35px;
    color: #fff;
    font-size: 20px;
    margin-top: -17.5px;
    line-height: 35px;
}
.pagedetail006 .relate-product-slick .owl-nav button.owl-prev span,.pagedetail006 .relate-product-slick .owl-nav button.owl-next span{
    display: inline;
}
.pagedetail006 .xypg-relate .relate-news{
    margin-top: 45px;
}
.pagedetail006 .relate-news-list li{
    float: left;
    width: 48%;
    position: relative;
    line-height: 2.143;
    font-size: 14px;
    color: #0d62be;
}
.pagedetail006 .relate-news-list li:nth-child(even){
    float: right;
}
.pagedetail006 .relate-news-list li a{
    display: block;
    padding-right: 125px;
    color: #666666;
}
.pagedetail006 .relate-news-list li a:before{
    content: '+';
    font-weight: bold;
    font-size: 16px;
    line-height: 1;
    margin-right: 5px;
}
.pagedetail006 .relate-news-list li span{
    position: absolute;
    right: 0;
    top: 2.5px;
    width: 125px;
    text-align: right;
    font-size: 12px;
    color: #666666;
}
.pagedetail006 .relate-news-list li:hover a{
    color: #0d62be;
}
.pagedetail006 .relate-news-list li:hover a:before{
    color: #0d62be;
}
.pagedetail006 .relate-news-list li:hover span{
    color: #0d62be;
}
@media (max-width: 990px){
.pagedetail006 .xypg-relate{
        display: none;
    }}
.pagedetail006 img{
    image-rendering: -moz-crisp-edges; /* Firefox */
    image-rendering: -o-crisp-edges; /* Opera */
    image-rendering: -webkit-optimize-contrast; /* Webkit (non-standard naming) */
    image-rendering: crisp-edges;
    -ms-interpolation-mode: nearest-neighbor; /* IE (non-standard property) */
}
.pagedetail006 .page-wrap-right p>img,.pagedetail006 .page-wrap-right span>img,.pagedetail006 .page-wrap-right strong>p{
    max-width: 100%!important;
    height: auto!important;
}
.pagedetail006 .tong{
  max-width: 1200px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 15px; }
.pagedetail006 p{
  margin: 0; }

</style>


<span class="pagedetail006">
    <div class="page-position">
        <div class="tong clearfix">您当前的位置 ：<%=navLocation(navid,"")%>
        </div>
    </div>
    <div class="page-wrap">
        <div class="tong clearfix">
            <div class="page-wrap-left xymob-menu-click">
                <div class="xymob-left-close-btn">
                    <i class="icon-font icon-close"></i>
                </div>
                <div class="xypg-left">
                    <div class="xypg-left-box xypg-left-menu">
                        <div class="xypg-left-title">
                            <h3><%=nav%>
                                <span><%=ennav%></span>
                            </h3>
                            <i>a</i>
                        </div>
                        <div class="xypg-left-con">
                            <ul class="xypg-left-nav">


 

 <%  
            if navid<>"" or parentid<>"" then
                if parentid=-1 then 
                    addsql=" and parentid=" & navid
                else
                    addsql=" and parentid=" & getRootNavId(parentid)
                end if
            end if
            rs.open "select * from " & db_PREFIX & "webcolumn where isthrough=1 "& addsql &" order by sortRank asc" ,conn,1,1
            if not rs.eof then
                while not rs.eof
            %>

                    <li class="<%=IIF(rs("columnname")=columnname,"clicked","")%>">
                        <a href="<%=getNavUrl(rs("id"),rs("columntype"))%>"><%=uTitle & rs("columnname")%></a>
                                    <div class="first-nav-btn"></div>
                    </li>

                    <%rs.movenext:wend
            else
            %>
                    <li class="clicked">
                        <a href="<%=getNavUrl(navid,columntype)%>"><%=uTitle & columnname%></a>
                                    <div class="first-nav-btn"></div>
                    </li>

            <%end if:rs.close%>



                       


                            </ul>
                        </div>
                    </div>
                    <div class="xypg-left-box xypg-left-news">
                        <div class="xypg-left-title">
                            <h3>新闻资讯
                                <span>News</span>
                            </h3>
                            <i>N</i>
                        </div>
                        <div class="xypg-left-con">
                            <ul class="latest-news">


<%
rs.open "select * from " & db_PREFIX & "ArticleDetail where isthrough=1 AND parentId in("& getNameToAllId("新闻资讯") &")order by views desc" ,conn,1,1
for i=1 to 6
if rs.eof then exit for
%>  
                        <li>
                            <a href="<%=getArticleUrl(rs("id"))%>"><%=uTitle & rs("title")%></a>
                        </li>
    <%rs.movenext:next:rs.close%>
 
                            </ul>
                        </div>
                    </div>
                        <%
                    '不需要
                    if 1=2 then%>
                    <div class="xypg-left-box xypg-left-keys">
                        <div class="xypg-left-title">
                            <h3>热门关键词
                                <span>Keywords</span>
                            </h3>
                            <i>K</i>
                        </div>
                        <div class="xypg-left-con">
                            <ul class="hot-keys">
                                <li>
                                    <a href="1111">111</a>
                                </li>                                 
                            </ul>
                        </div>
                    </div>
                    <%end if%>
                    <div class="xypg-left-box xypg-left-contact">
                        <div class="xypg-left-title">
                            <h3>联系我们
                                <span>Contact Us</span>
                            </h3>
                            <i>C</i>
                        </div>
                        <div class="xypg-left-con">
                            <div class="page-wrap-contact">
                                <h4><%=webcompany%></h4>
                                <p>电 话：<%=webtel%></p>
                                <p>传 真：<%=webfax%></p>
                                <p>邮 箱：<%=webemail%></p>
                                <p>地 址：<%=webaddress%></p>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="page-wrap-right">
                <div class="xypg-right-content">
                    <!-- 新闻详细 -->
                    <div class="xypg-news-detail">
                        <h1 class="xypg-detail-title"><%=title%></h1>
                        <div class="xypg-detail-info-bar">
                            <div class="detail-info-time">
                                <i class="icon-font icon-shijian"></i><%=createtime%>
                            </div>
                            <div class="detail-info-numbers">
                                <i class="icon-font icon-chakan"></i>
                                <script src="js/browse.js"></script><%=views%>次
                            </div>
                        </div>
                        <div class="xypg-detail-con">
                            
                            <%
                            '大图存在则显示大图，如果大图为#则不显示图片20220519'
                            if IIF(bigimage<>"",bigimage,smallimage)<>"#" then%>
                            <p style="text-align: center;">
                                <img src="<%=IIF(bigimage<>"",bigimage,smallimage)%>" class="img-responsive center-block" /></p>
                            <%end if%>
                    <p style="line-height:28px;"> 
 

                        <%=bodyContent%></p>


                        </div> 
                        <div class="xypg-detail-pn">
                            <div>
                                <b>上一篇：</b>
<% 
    if id<>"" then
        addsql=""
        if parentId<>-1 then addsql=" and parentId="& parentId
        'call echo("addsql",addsql)
    rsx.open"select * from " & db_PREFIX & "ArticleDetail where id<"& id & addsql &" order by id desc"  ,conn,1,1
        if rsx.eof then
            response.Write("没有了<span>"& format_Time(rs("createtime"),2) &"</span>")
        else
            response.Write("<a href="""& getArticleUrl(rsx("id")) &""">"&uTitle& rsx("title") &"</a><span>"& format_Time(rs("createtime"),2) &"</span>")
        end if:rsx.close
        
        
    end if
    %>
                                
                            </div>



                            <div>
                                <b>下一篇：</b>

<% 
    if id<>"" then
        addsql=""
        if parentId<>-1 then addsql=" and parentId="& parentId
        
        
    rsx.open"select * from " & db_PREFIX & "ArticleDetail where id>"& id & addsql &" order by id asc"  ,conn,1,1
        if rsx.eof then
            response.Write("没有了<span>"& format_Time(rs("createtime"),2) &"</span>")
        else        
            response.Write("<a href="""& getArticleUrl(rsx("id")) &""">"&uTitle& rsx("title") &"</a><span>"& format_Time(rs("createtime"),2) &"</span>")
        end if:rsx.close
    end if
    %>

                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="page-mob-tool">
                <ul>
                    <li class="xymob-page-navbtn">
                        <i class="icon-font icon-dots-horizontal"></i>
                    </li>
                    <li class="xymob-page-backtop" onclick="$('html,body').animate({scrollTop: '0px'}, 500);">
                        <i class="icon-font icon-top"></i>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<script>
$(function(){

    // 内页手机端端 做的导航弹出特效
    $(".xymob-page-navbtn").click(function(){
        $(".xymob-menu-click").addClass('click');
        $('html,body').addClass('no-scroll');
    });
    $(".xymob-left-close-btn").click(function(){
        $(".xymob-menu-click").removeClass('click');
        $('html,body').removeClass('no-scroll');
    });
})    
</script>
</span>
