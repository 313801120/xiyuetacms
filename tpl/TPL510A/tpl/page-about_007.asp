﻿<% 
'#禁止自动更新当前文件'  #号去掉代表此文件不被程序自动更新替换掉

                 '''
'               (0 0)
'   +-----oOO----(_)------------+
'   |                           |
'   |    author:XIYUETA         |
'   |    QQ:313801120           |
'   |    www.xiyueta.com        |
'   |                           |
'   +------------------oOO------+
'              |__|__|
'               || ||
'              ooO Ooo

resurl="/web/tpl/page-about/007/"
 %>
<style>

.pageabout007 *{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    transition:none 0.4s linear;
    -webkit-transition:none 0.4s linear;
    -moz-transition:none 0.4s linear;
}
.pageabout007 *,.pageabout007 ::after,.pageabout007 ::before{box-sizing: border-box;}
.pageabout007 html{
    font-size: 10px; /*10 ÷ 16 × 100% = 62.5%*/
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
}
.pageabout007 body{
    font-size: 1.4rem;
    font-size: 14px;
    margin: 0 auto;
    font-family: "Microsoft Yahei", "Helvetica Neue", Helvetica, Arial, sans-serif;
    -ms-behavior: url(<%=resUrl%>images/backgroundsize.min.jpg);
    behavior: url(<%=resUrl%>images/backgroundsize.min.jpg);
    line-height: 1.5;
    color: #444;
}
.pageabout007 div,.pageabout007 span,.pageabout007 applet,.pageabout007 object,.pageabout007 iframe,.pageabout007 h1,.pageabout007 h2,.pageabout007 h3,.pageabout007 h4,.pageabout007 h5,.pageabout007 h6,.pageabout007 p,.pageabout007 blockquote,.pageabout007 pre,.pageabout007 a,.pageabout007 abbr,.pageabout007 acronym,.pageabout007 address,.pageabout007 big,.pageabout007 cite,.pageabout007 code,.pageabout007 del,.pageabout007 dfn,.pageabout007 em,.pageabout007 img,.pageabout007 ins,.pageabout007 kbd,.pageabout007 q,.pageabout007 s,.pageabout007 samp,.pageabout007 small,.pageabout007 strike,.pageabout007 strong,.pageabout007 sub,.pageabout007 sup,.pageabout007 tt,.pageabout007 var,.pageabout007 b,.pageabout007 u,.pageabout007 i,.pageabout007 center,.pageabout007 dl,.pageabout007 dt,.pageabout007 dd,.pageabout007 ol,.pageabout007 ul,.pageabout007 li,.pageabout007 fieldset,.pageabout007 form,.pageabout007 label,.pageabout007 legend,.pageabout007 table,.pageabout007 caption,.pageabout007 tbody,.pageabout007 tfoot,.pageabout007 thead,.pageabout007 tr,.pageabout007 th,.pageabout007 td,.pageabout007 article,.pageabout007 aside,.pageabout007 canvas,.pageabout007 details,.pageabout007 embed,.pageabout007 figure,.pageabout007 figcaption,.pageabout007 footer,.pageabout007 header,.pageabout007 hgroup,.pageabout007 menu,.pageabout007 nav,.pageabout007 output,.pageabout007 ruby,.pageabout007 section,.pageabout007 summary,.pageabout007 time,.pageabout007 mark,.pageabout007 audio,.pageabout007 video{
    margin:0; padding:0; border:0;outline:0;font-family: inherit;font-size: inherit;line-height:inherit;color:inherit;vertical-align:baseline;
}
.pageabout007 li{list-style:none;}
.pageabout007 img{max-width: 100%;vertical-align:middle;}
.pageabout007 a{color:inherit;text-decoration: none;}
.pageabout007 a:active,.pageabout007 a:hover{outline: 0;}
.pageabout007 a:hover,.pageabout007 a:focus{text-decoration: none;color:inherit}
.pageabout007 img{border:0;}
.pageabout007 b,.pageabout007 strong{font-weight: bold;}
.pageabout007 .text-gray{color: #999;}
.pageabout007 .clearfix:before,.pageabout007 .clearfix:after,.pageabout007 .dl-horizontal dd:before,.pageabout007 .dl-horizontal dd:after,.pageabout007 .container:before,.pageabout007 .container:after,.pageabout007 .container-fluid:before,.pageabout007 .container-fluid:after,.pageabout007 .row:before,.pageabout007 .row:after,.pageabout007 .form-horizontal .form-group:before,.pageabout007 .form-horizontal .form-group:after,.pageabout007 .btn-toolbar:before,.pageabout007 .btn-toolbar:after,.pageabout007 .btn-group-vertical > .btn-group:before,.pageabout007 .btn-group-vertical > .btn-group:after,.pageabout007 .nav:before,.pageabout007 .nav:after,.pageabout007 .navbar:before,.pageabout007 .navbar:after,.pageabout007 .navbar-header:before,.pageabout007 .navbar-header:after,.pageabout007 .navbar-collapse:before,.pageabout007 .navbar-collapse:after,.pageabout007 .pager:before,.pageabout007 .pager:after,.pageabout007 .panel-body:before,.pageabout007 .panel-body:after,.pageabout007 .modal-header:before,.pageabout007 .modal-header:after,.pageabout007 .modal-footer:before,.pageabout007 .modal-footer:after{content: " ";display: table;}
.pageabout007 .clearfix:after,.pageabout007 .dl-horizontal dd:after,.pageabout007 .container:after,.pageabout007 .container-fluid:after,.pageabout007 .row:after,.pageabout007 .form-horizontal .form-group:after,.pageabout007 .btn-toolbar:after,.pageabout007 .btn-group-vertical > .btn-group:after,.pageabout007 .nav:after,.pageabout007 .navbar:after,.pageabout007 .navbar-header:after,.pageabout007 .navbar-collapse:after,.pageabout007 .pager:after,.pageabout007 .panel-body:after,.pageabout007 .modal-header:after,.pageabout007 .modal-footer:after{clear: both;}
.pageabout007 .visible-xs,.pageabout007 .visible-sm,.pageabout007 .visible-md,.pageabout007 .visible-lg{display: none !important;}
.pageabout007 .text-uppercase{text-transform: uppercase;}
.pageabout007 .position-relative{position: relative}
.pageabout007 .container{margin-right: auto;margin-left: auto;padding-left: 15px;padding-right: 15px;}
.pageabout007 .container .container{padding-left:0px;padding-right:0px}
@media (min-width:768px){
.pageabout007 .container{width: 100%;}}
@media (min-width:992px){
.pageabout007 .container{width:100%;}}
.pageabout007 .col-xs-1,.pageabout007 .col-sm-1,.pageabout007 .col-md-1,.pageabout007 .col-lg-1,.pageabout007 .col-xs-2,.pageabout007 .col-sm-2,.pageabout007 .col-md-2,.pageabout007 .col-lg-2,.pageabout007 .col-xs-3,.pageabout007 .col-sm-3,.pageabout007 .col-md-3,.pageabout007 .col-lg-3,.pageabout007 .col-xs-4,.pageabout007 .col-sm-4,.pageabout007 .col-md-4,.pageabout007 .col-lg-4,.pageabout007 .col-xs-5,.pageabout007 .col-sm-5,.pageabout007 .col-md-5,.pageabout007 .col-lg-5,.pageabout007 .col-xs-6,.pageabout007 .col-sm-6,.pageabout007 .col-md-6,.pageabout007 .col-lg-6,.pageabout007 .col-xs-7,.pageabout007 .col-sm-7,.pageabout007 .col-md-7,.pageabout007 .col-lg-7,.pageabout007 .col-xs-8,.pageabout007 .col-sm-8,.pageabout007 .col-md-8,.pageabout007 .col-lg-8,.pageabout007 .col-xs-9,.pageabout007 .col-sm-9,.pageabout007 .col-md-9,.pageabout007 .col-lg-9,.pageabout007 .col-xs-10,.pageabout007 .col-sm-10,.pageabout007 .col-md-10,.pageabout007 .col-lg-10,.pageabout007 .col-xs-11,.pageabout007 .col-sm-11,.pageabout007 .col-md-11,.pageabout007 .col-lg-11,.pageabout007 .col-xs-12,.pageabout007 .col-sm-12,.pageabout007 .col-md-12,.pageabout007 .col-lg-12,.pageabout007 .col-xs-avg-5,.pageabout007 .col-xs-avg-4,.pageabout007 .col-xs-avg-3,.pageabout007 .col-xs-avg-2,.pageabout007 .col-xs-avg-1,.pageabout007 .col-sm-avg-5,.pageabout007 .col-sm-avg-4,.pageabout007 .col-sm-avg-3,.pageabout007 .col-sm-avg-2,.pageabout007 .col-sm-avg-1,.pageabout007 .col-md-avg-5,.pageabout007 .col-md-avg-4,.pageabout007 .col-md-avg-3,.pageabout007 .col-md-avg-2,.pageabout007 .col-md-avg-1,.pageabout007 .col-lg-avg-5,.pageabout007 .col-lg-avg-4,.pageabout007 .col-lg-avg-3,.pageabout007 .col-lg-avg-2,.pageabout007 .col-lg-avg-1{
    position: relative;
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
}
.pageabout007 .col-xs-1,.pageabout007 .col-xs-2,.pageabout007 .col-xs-3,.pageabout007 .col-xs-4,.pageabout007 .col-xs-5,.pageabout007 .col-xs-6,.pageabout007 .col-xs-7,.pageabout007 .col-xs-8,.pageabout007 .col-xs-9,.pageabout007 .col-xs-10,.pageabout007 .col-xs-11,.pageabout007 .col-xs-12,.pageabout007 .col-xs-avg-5,.pageabout007 .col-xs-avg-4,.pageabout007 .col-xs-avg-3,.pageabout007 .col-xs-avg-2,.pageabout007 .col-xs-avg-1{
    float: left;
}
.pageabout007 .col-xs-9{width: 75%;}
.pageabout007 .col-xs-3{width: 25%;}
@media (max-width: 767px){
.pageabout007 .hidden-xs{display: none !important;}}
@media (min-width: 768px){
.pageabout007 .col-sm-1,.pageabout007 .col-sm-2,.pageabout007 .col-sm-3,.pageabout007 .col-sm-4,.pageabout007 .col-sm-5,.pageabout007 .col-sm-6,.pageabout007 .col-sm-7,.pageabout007 .col-sm-8,.pageabout007 .col-sm-9,.pageabout007 .col-sm-10,.pageabout007 .col-sm-11,.pageabout007 .col-sm-12,.pageabout007 .col-sm-avg-5,.pageabout007 .col-sm-avg-4,.pageabout007 .col-sm-avg-3,.pageabout007 .col-sm-avg-2,.pageabout007 .col-sm-avg-1{
        float: left;
    }
.pageabout007 .col-sm-9{width: 75%;}
.pageabout007 .col-sm-3{width: 25%;}
.pageabout007 .hidden-sm{display: none !important;}}
@media (min-width: 992px){
.pageabout007 .col-md-1,.pageabout007 .col-md-2,.pageabout007 .col-md-3,.pageabout007 .col-md-4,.pageabout007 .col-md-5,.pageabout007 .col-md-6,.pageabout007 .col-md-7,.pageabout007 .col-md-8,.pageabout007 .col-md-9,.pageabout007 .col-md-10,.pageabout007 .col-md-11,.pageabout007 .col-md-12,.pageabout007 .col-md-avg-5,.pageabout007 .col-md-avg-4,.pageabout007 .col-md-avg-3,.pageabout007 .col-md-avg-2,.pageabout007 .col-md-avg-1{
        float: left;
    }
.pageabout007 .col-md-9{width: 75%;}
.pageabout007 .col-md-3{width: 25%;}
.pageabout007 .visible-md{display: block !important;}}







.pageabout007 button,.pageabout007 html input[type="button"],.pageabout007 input[type="reset"],.pageabout007 input[type="submit"]{-webkit-appearance: button;cursor: pointer;}
.pageabout007 button[disabled],.pageabout007 html input[disabled]{cursor: default;}
.pageabout007 .font-size-16{font-size: 16px}
.pageabout007 .font-size-22{font-size: 22px}
.pageabout007 .padding-top-20{padding-top: 20px}
.pageabout007 .padding-bottom-10{padding-bottom: 10px}
.pageabout007 .padding-left-0{ padding-left: 0px;}
.pageabout007 .container{max-width:1200px;}
.pageabout007 .info-content{font-size:15px;line-height:2.5;}
.pageabout007 a{color:#333;text-decoration:none;}
.pageabout007 a:hover{color:#0062d0}
.pageabout007 a:focus{color:#0062d0}
.pageabout007 .nav-fenye{height:auto;width:100%;margin-top:30px;}
.pageabout007 .nav-content{width:100%;/*height:100%;*/ overflow-wrap: break-word;padding: 0px;border-color: #0d62be;border-width: 2px;background-color: transparent;border-style: solid;}
.pageabout007 .nav-header{width:100%;height:auto;background-color: #0d62be;}
.pageabout007 .header-wenzi{padding-top: 20px;width:100%;display: block; font-family: "Microsoft YaHei";font-size: 30px;color: rgb(255, 255, 255);text-align:center;}
.pageabout007 .header-wenzi1{padding-bottom: 20px;font-family: "Microsoft YaHei";font-size: 22px;color: rgb(255, 255, 255);width:100%;display: block;text-align: center; }
.pageabout007 .header-top{left: 8px;top: 17px;position: absolute;width: 34px;height: 34px;transform: rotate(31deg);transform-origin: 0px 0px;}
.pageabout007 .top-content{margin: 15px 0; border: none;border-top-color: currentcolor;border-top-style: none;border-top-width: medium;border-top: 2px solid #ffffff;height: 0;}
.pageabout007 .footer-content{margin: 15px 0; border: none;border-top-color: currentcolor;border-top-style: none;border-top-width: medium;border-top: 2px solid #ffffff;height: 0;}
.pageabout007 .header-footer{right: -11px;top: 117px;position: absolute;z-index: 137;transform: rotate(31deg);transform-origin: 0px 0px;padding: 0px;border-color: transparent;border-width: 0px;overflow: hidden;width: 34px;height: 34px;}
.pageabout007 .nav-footer{padding: 10px;clear: both;overflow: hidden;overflow-wrap: break-word;border-color: transparent;border-width: 0px;height: auto;background-color: #0d62be;width: 100%;border-style: solid;}
.pageabout007 .footer-img{ overflow: hidden;height: auto;border-width: 0px;border-style: solid;padding: 6px 10px 0px;}
.pageabout007 .footer-img img{width:42px;height:auto;}
.pageabout007 .footer-title{font-size: 14px;font-family: "Microsoft YaHei";color: rgb(255, 255, 255);}
.pageabout007 .footer-wenzi{float:left;}
.pageabout007 .footer-wenzi strong{font-size: 20px;font-family: "Microsoft YaHei";color: #fff;}
.pageabout007 .footer-wenzi span{overflow: hidden;}
.pageabout007 .nav-box{clear:both;text-align:left;margin-bottom:20px;width:100%;overflow:visible;}
.pageabout007 .nav-box li{width:100%;padding:0px;text-align:left;position:relative;display:block;}
.pageabout007 .nav-box li a{display:block;text-align:center;line-height:50px;color:#fff;font-family:"Microsoft YaHei",Tahoma,Verdana,"Simsun";font-size:16px;background-color: #0d62be;margin: 20px 20px;}
.pageabout007 .nav-box li a:hover,.pageabout007 .nav-box li.current > a{background-color: #326ec0;}
@media (min-width:1000px){
.pageabout007 .wordsmax{margin:40px 0 20px;}
.pageabout007 .wordsmax:after{width:12%;}}
@media (max-width:1000px){
.pageabout007 .wordsmax{margin:-24px 0 30px;}
.pageabout007 .wordsmax:after{width:30%;}
.pageabout007 .wordsmax .fontsize{font-size:18px!important;}
.pageabout007 .wordsmax .text{font-size:14px!important;}
.pageabout007 .footer-wenzi span{font-size: 13px;}}
.pageabout007 .wordsmax{position: relative;left: 0;z-index: 1;top: 0;width:100%;border-bottom:1px solid #d9d9d9;}
.pageabout007 .wordsmax .fontsize{border-left:2px solid #0d62be;padding-left:10px;}
.pageabout007 .wordsmax:after{content:"";height:3px;background-color:#0d62be;display:block;}
@media (max-width:1000px){
.pageabout007 .addphone{padding-left: 0;padding-right: 0;}}

</style>

<span class="pageabout007">
    <div class="container" style="margin-bottom:40px;">
        <div class="col-md-3 padding-top-20">
            <div class="container position-relative visible-md hidden-sm hidden-xs">
                <div class="nav-fenye">
                    <div class="nav-content">
                        <div class="nav-header">
                            <span class="header-wenzi text-uppercase"><%=ennav%></span>
                            <div class="header-top">
                                <div class="top-content"></div>
                            </div>
                            <span class="header-wenzi1"><%=nav%></span>
                            <div class="header-footer">
                                <div class="footer-content"></div>
                            </div>
                        </div>
                        <div class="nav-box" id="subNavs">
                            <ul>
 

 <%  
            if navid<>"" or parentid<>"" then
                if parentid=-1 then 
                    addsql=" and parentid=" & navid
                else
                    addsql=" and parentid=" & getRootNavId(parentid)
                end if
            end if
            rs.open "select * from " & db_PREFIX & "webcolumn where isthrough=1 "& addsql &" order by sortRank asc" ,conn,1,1
            if not rs.eof then
                while not rs.eof
            %>

                    <li class="<%=IIF(rs("columnname")=columnname,"current active","")%>">
                        <a href="<%=getNavUrl(rs("id"),rs("columntype"))%>"><%=uTitle & rs("columnname")%></a>
                                    <div class="first-nav-btn"></div>
                    </li>

                    <%rs.movenext:wend
            else
            %>
                    <li class="current active">
                        <a href="<%=getNavUrl(navid,columntype)%>"><%=uTitle & columnname%></a>
                                    <div class="first-nav-btn"></div>
                    </li>

            <%end if:rs.close%>

                            </ul>
                        </div>
                        <div class="nav-footer ">
                            <div class="footer-img col-md-3 col-sm-3 col-xs-3"><img src="<%=resUrl%>images/t7.png">
                            </div>
                            <div class="footer-wenzi col-md-9 col-sm-9 col-xs-9 padding-left-0">
                                <span class="footer-title">全国服务热线：</span><br>
                                <strong><%=webphone%></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="col-md-9 padding-top-20 addphone">
            <div class="wordsmax">
                <p class="padding-bottom-10">
                    <span class="fontsize font-size-22"><%=uTitle & columnname%></span>/
                    <span class="font-size-16 text-gray text-uppercase text"><%=uTitle & columnenname%></span>
                </p>
            </div>
            <div class="info-content">
                <p>
                   <%=bodyContent%>
                   </p>
            </div>
        </div>
    </div>    
</span>
