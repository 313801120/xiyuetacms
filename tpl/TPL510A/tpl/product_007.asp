﻿<% 
'#禁止自动更新当前文件'  #号去掉代表此文件不被程序自动更新替换掉

                 '''
'               (0 0)
'   +-----oOO----(_)------------+
'   |                           |
'   |    author:XIYUETA         |
'   |    QQ:313801120           |
'   |    www.xiyueta.com        |
'   |                           |
'   +------------------oOO------+
'              |__|__|
'               || ||
'              ooO Ooo

cssName="product007" 
resurl="/web/tpl/product/007/"
 %>
<style>

.warp<%=cssName%> *{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    transition:none 0.4s linear;
    -webkit-transition:none 0.4s linear;
    -moz-transition:none 0.4s linear;
}
.warp<%=cssName%> *,.warp<%=cssName%> ::after,.warp<%=cssName%> ::before{box-sizing: border-box;}
.warp<%=cssName%> html{
    font-size: 10px; /*10 ÷ 16 × 100% = 62.5%*/
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
}
.warp<%=cssName%> body{
    font-size: 1.4rem;
    font-size: 14px;
    margin: 0 auto;
    font-family: "Microsoft Yahei", "Helvetica Neue", Helvetica, Arial, sans-serif;
    -ms-behavior: url(<%=resUrl%>images/backgroundsize.min.jpg);
    behavior: url(<%=resUrl%>images/backgroundsize.min.jpg);
    line-height: 1.5;
    color: #444;
}
.warp<%=cssName%> div,.warp<%=cssName%> span,.warp<%=cssName%> applet,.warp<%=cssName%> object,.warp<%=cssName%> iframe,.warp<%=cssName%> h1,.warp<%=cssName%> h2,.warp<%=cssName%> h3,.warp<%=cssName%> h4,.warp<%=cssName%> h5,.warp<%=cssName%> h6,.warp<%=cssName%> p,.warp<%=cssName%> blockquote,.warp<%=cssName%> pre,.warp<%=cssName%> a,.warp<%=cssName%> abbr,.warp<%=cssName%> acronym,.warp<%=cssName%> address,.warp<%=cssName%> big,.warp<%=cssName%> cite,.warp<%=cssName%> code,.warp<%=cssName%> del,.warp<%=cssName%> dfn,.warp<%=cssName%> em,.warp<%=cssName%> img,.warp<%=cssName%> ins,.warp<%=cssName%> kbd,.warp<%=cssName%> q,.warp<%=cssName%> s,.warp<%=cssName%> samp,.warp<%=cssName%> small,.warp<%=cssName%> strike,.warp<%=cssName%> strong,.warp<%=cssName%> sub,.warp<%=cssName%> sup,.warp<%=cssName%> tt,.warp<%=cssName%> var,.warp<%=cssName%> b,.warp<%=cssName%> u,.warp<%=cssName%> i,.warp<%=cssName%> center,.warp<%=cssName%> dl,.warp<%=cssName%> dt,.warp<%=cssName%> dd,.warp<%=cssName%> ol,.warp<%=cssName%> ul,.warp<%=cssName%> li,.warp<%=cssName%> fieldset,.warp<%=cssName%> form,.warp<%=cssName%> label,.warp<%=cssName%> legend,.warp<%=cssName%> table,.warp<%=cssName%> caption,.warp<%=cssName%> tbody,.warp<%=cssName%> tfoot,.warp<%=cssName%> thead,.warp<%=cssName%> tr,.warp<%=cssName%> th,.warp<%=cssName%> td,.warp<%=cssName%> article,.warp<%=cssName%> aside,.warp<%=cssName%> canvas,.warp<%=cssName%> details,.warp<%=cssName%> embed,.warp<%=cssName%> figure,.warp<%=cssName%> figcaption,.warp<%=cssName%> footer,.warp<%=cssName%> header,.warp<%=cssName%> hgroup,.warp<%=cssName%> menu,.warp<%=cssName%> nav,.warp<%=cssName%> output,.warp<%=cssName%> ruby,.warp<%=cssName%> section,.warp<%=cssName%> summary,.warp<%=cssName%> time,.warp<%=cssName%> mark,.warp<%=cssName%> audio,.warp<%=cssName%> video{
    margin:0; padding:0; border:0;outline:0;font-family: inherit;font-size: inherit;line-height:inherit;color:inherit;vertical-align:baseline;
}
.warp<%=cssName%> article,.warp<%=cssName%> aside,.warp<%=cssName%> details,.warp<%=cssName%> figcaption,.warp<%=cssName%> figure,.warp<%=cssName%> footer,.warp<%=cssName%> header,.warp<%=cssName%> hgroup,.warp<%=cssName%> main,.warp<%=cssName%> menu,.warp<%=cssName%> nav,.warp<%=cssName%> section,.warp<%=cssName%> summary{display: block;}
.warp<%=cssName%> li{list-style:none;}
.warp<%=cssName%> img{max-width: 100%;vertical-align:middle;}
.warp<%=cssName%> a{color:inherit;text-decoration: none;}
.warp<%=cssName%> a:active,.warp<%=cssName%> a:hover{outline: 0;}
.warp<%=cssName%> a:hover,.warp<%=cssName%> a:focus{text-decoration: none;color:inherit}
.warp<%=cssName%> img{border:0;}
.warp<%=cssName%> a img{border:0;}
.warp<%=cssName%> b,.warp<%=cssName%> strong{font-weight: bold;}
.warp<%=cssName%> .text-gray{color: #999;}
.warp<%=cssName%> .clearfix:before,.warp<%=cssName%> .clearfix:after,.warp<%=cssName%> .dl-horizontal dd:before,.warp<%=cssName%> .dl-horizontal dd:after,.warp<%=cssName%> .container:before,.warp<%=cssName%> .container:after,.warp<%=cssName%> .container-fluid:before,.warp<%=cssName%> .container-fluid:after,.warp<%=cssName%> .row:before,.warp<%=cssName%> .row:after,.warp<%=cssName%> .form-horizontal .form-group:before,.warp<%=cssName%> .form-horizontal .form-group:after,.warp<%=cssName%> .btn-toolbar:before,.warp<%=cssName%> .btn-toolbar:after,.warp<%=cssName%> .btn-group-vertical > .btn-group:before,.warp<%=cssName%> .btn-group-vertical > .btn-group:after,.warp<%=cssName%> .nav:before,.warp<%=cssName%> .nav:after,.warp<%=cssName%> .navbar:before,.warp<%=cssName%> .navbar:after,.warp<%=cssName%> .navbar-header:before,.warp<%=cssName%> .navbar-header:after,.warp<%=cssName%> .navbar-collapse:before,.warp<%=cssName%> .navbar-collapse:after,.warp<%=cssName%> .pager:before,.warp<%=cssName%> .pager:after,.warp<%=cssName%> .panel-body:before,.warp<%=cssName%> .panel-body:after,.warp<%=cssName%> .modal-header:before,.warp<%=cssName%> .modal-header:after,.warp<%=cssName%> .modal-footer:before,.warp<%=cssName%> .modal-footer:after{content: " ";display: table;}
.warp<%=cssName%> .clearfix:after,.warp<%=cssName%> .dl-horizontal dd:after,.warp<%=cssName%> .container:after,.warp<%=cssName%> .container-fluid:after,.warp<%=cssName%> .row:after,.warp<%=cssName%> .form-horizontal .form-group:after,.warp<%=cssName%> .btn-toolbar:after,.warp<%=cssName%> .btn-group-vertical > .btn-group:after,.warp<%=cssName%> .nav:after,.warp<%=cssName%> .navbar:after,.warp<%=cssName%> .navbar-header:after,.warp<%=cssName%> .navbar-collapse:after,.warp<%=cssName%> .pager:after,.warp<%=cssName%> .panel-body:after,.warp<%=cssName%> .modal-header:after,.warp<%=cssName%> .modal-footer:after{clear: both;}
.warp<%=cssName%> .visible-xs,.warp<%=cssName%> .visible-sm,.warp<%=cssName%> .visible-md,.warp<%=cssName%> .visible-lg{display: none !important;}
.warp<%=cssName%> .text-center{text-align:center}
.warp<%=cssName%> .text-uppercase{text-transform: uppercase;}
.warp<%=cssName%> .position-relative{position: relative}
.warp<%=cssName%> .container{margin-right: auto;margin-left: auto;padding-left: 15px;padding-right: 15px;}
.warp<%=cssName%> .container .container{padding-left:0px;padding-right:0px}
@media (min-width:768px){
.warp<%=cssName%> .container{width: 100%;}}
@media (min-width:992px){
.warp<%=cssName%> .container{width:100%;}}
.warp<%=cssName%> .row{margin-left: -15px;margin-right: -15px;}
.warp<%=cssName%> .row .row{margin-left:0px;margin-right:0px}
.warp<%=cssName%> .col-xs-1,.warp<%=cssName%> .col-sm-1,.warp<%=cssName%> .col-md-1,.warp<%=cssName%> .col-lg-1,.warp<%=cssName%> .col-xs-2,.warp<%=cssName%> .col-sm-2,.warp<%=cssName%> .col-md-2,.warp<%=cssName%> .col-lg-2,.warp<%=cssName%> .col-xs-3,.warp<%=cssName%> .col-sm-3,.warp<%=cssName%> .col-md-3,.warp<%=cssName%> .col-lg-3,.warp<%=cssName%> .col-xs-4,.warp<%=cssName%> .col-sm-4,.warp<%=cssName%> .col-md-4,.warp<%=cssName%> .col-lg-4,.warp<%=cssName%> .col-xs-5,.warp<%=cssName%> .col-sm-5,.warp<%=cssName%> .col-md-5,.warp<%=cssName%> .col-lg-5,.warp<%=cssName%> .col-xs-6,.warp<%=cssName%> .col-sm-6,.warp<%=cssName%> .col-md-6,.warp<%=cssName%> .col-lg-6,.warp<%=cssName%> .col-xs-7,.warp<%=cssName%> .col-sm-7,.warp<%=cssName%> .col-md-7,.warp<%=cssName%> .col-lg-7,.warp<%=cssName%> .col-xs-8,.warp<%=cssName%> .col-sm-8,.warp<%=cssName%> .col-md-8,.warp<%=cssName%> .col-lg-8,.warp<%=cssName%> .col-xs-9,.warp<%=cssName%> .col-sm-9,.warp<%=cssName%> .col-md-9,.warp<%=cssName%> .col-lg-9,.warp<%=cssName%> .col-xs-10,.warp<%=cssName%> .col-sm-10,.warp<%=cssName%> .col-md-10,.warp<%=cssName%> .col-lg-10,.warp<%=cssName%> .col-xs-11,.warp<%=cssName%> .col-sm-11,.warp<%=cssName%> .col-md-11,.warp<%=cssName%> .col-lg-11,.warp<%=cssName%> .col-xs-12,.warp<%=cssName%> .col-sm-12,.warp<%=cssName%> .col-md-12,.warp<%=cssName%> .col-lg-12,.warp<%=cssName%> .col-xs-avg-5,.warp<%=cssName%> .col-xs-avg-4,.warp<%=cssName%> .col-xs-avg-3,.warp<%=cssName%> .col-xs-avg-2,.warp<%=cssName%> .col-xs-avg-1,.warp<%=cssName%> .col-sm-avg-5,.warp<%=cssName%> .col-sm-avg-4,.warp<%=cssName%> .col-sm-avg-3,.warp<%=cssName%> .col-sm-avg-2,.warp<%=cssName%> .col-sm-avg-1,.warp<%=cssName%> .col-md-avg-5,.warp<%=cssName%> .col-md-avg-4,.warp<%=cssName%> .col-md-avg-3,.warp<%=cssName%> .col-md-avg-2,.warp<%=cssName%> .col-md-avg-1,.warp<%=cssName%> .col-lg-avg-5,.warp<%=cssName%> .col-lg-avg-4,.warp<%=cssName%> .col-lg-avg-3,.warp<%=cssName%> .col-lg-avg-2,.warp<%=cssName%> .col-lg-avg-1{
    position: relative;
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
}
.warp<%=cssName%> .col-xs-1,.warp<%=cssName%> .col-xs-2,.warp<%=cssName%> .col-xs-3,.warp<%=cssName%> .col-xs-4,.warp<%=cssName%> .col-xs-5,.warp<%=cssName%> .col-xs-6,.warp<%=cssName%> .col-xs-7,.warp<%=cssName%> .col-xs-8,.warp<%=cssName%> .col-xs-9,.warp<%=cssName%> .col-xs-10,.warp<%=cssName%> .col-xs-11,.warp<%=cssName%> .col-xs-12,.warp<%=cssName%> .col-xs-avg-5,.warp<%=cssName%> .col-xs-avg-4,.warp<%=cssName%> .col-xs-avg-3,.warp<%=cssName%> .col-xs-avg-2,.warp<%=cssName%> .col-xs-avg-1{
    float: left;
}
.warp<%=cssName%> .col-xs-9{width: 75%;}
.warp<%=cssName%> .col-xs-6{width: 50%;}
.warp<%=cssName%> .col-xs-3{width: 25%;}
@media (max-width: 767px){
.warp<%=cssName%> .hidden-xs{display: none !important;}
.warp<%=cssName%> .visible-xs{display: block !important;}}
@media (min-width: 768px){
.warp<%=cssName%> .col-sm-1,.warp<%=cssName%> .col-sm-2,.warp<%=cssName%> .col-sm-3,.warp<%=cssName%> .col-sm-4,.warp<%=cssName%> .col-sm-5,.warp<%=cssName%> .col-sm-6,.warp<%=cssName%> .col-sm-7,.warp<%=cssName%> .col-sm-8,.warp<%=cssName%> .col-sm-9,.warp<%=cssName%> .col-sm-10,.warp<%=cssName%> .col-sm-11,.warp<%=cssName%> .col-sm-12,.warp<%=cssName%> .col-sm-avg-5,.warp<%=cssName%> .col-sm-avg-4,.warp<%=cssName%> .col-sm-avg-3,.warp<%=cssName%> .col-sm-avg-2,.warp<%=cssName%> .col-sm-avg-1{
        float: left;
    }
.warp<%=cssName%> .col-sm-9{width: 75%;}
.warp<%=cssName%> .col-sm-3{width: 25%;}
.warp<%=cssName%> .hidden-sm{display: none !important;}}
@media (min-width: 992px){
.warp<%=cssName%> .col-md-1,.warp<%=cssName%> .col-md-2,.warp<%=cssName%> .col-md-3,.warp<%=cssName%> .col-md-4,.warp<%=cssName%> .col-md-5,.warp<%=cssName%> .col-md-6,.warp<%=cssName%> .col-md-7,.warp<%=cssName%> .col-md-8,.warp<%=cssName%> .col-md-9,.warp<%=cssName%> .col-md-10,.warp<%=cssName%> .col-md-11,.warp<%=cssName%> .col-md-12,.warp<%=cssName%> .col-md-avg-5,.warp<%=cssName%> .col-md-avg-4,.warp<%=cssName%> .col-md-avg-3,.warp<%=cssName%> .col-md-avg-2,.warp<%=cssName%> .col-md-avg-1{
        float: left;
    }
.warp<%=cssName%> .col-md-9{width: 75%;}
.warp<%=cssName%> .col-md-3{width: 25%;}
.warp<%=cssName%> .hidden-md{display: none !important;}
.warp<%=cssName%> .visible-md{display: block !important;}}


.warp<%=cssName%> button,.warp<%=cssName%> html input[type="button"],.warp<%=cssName%> input[type="reset"],.warp<%=cssName%> input[type="submit"]{-webkit-appearance: button;cursor: pointer;}
.warp<%=cssName%> button[disabled],.warp<%=cssName%> html input[disabled]{cursor: default;}
.warp<%=cssName%> .line-height-2{ line-height:2;}
.warp<%=cssName%> .font-size-16{font-size: 16px}
.warp<%=cssName%> .font-size-22{font-size: 22px}
.warp<%=cssName%> .padding-top-20{padding-top: 20px}
.warp<%=cssName%> .padding-right-2{padding-right: 2px}
.warp<%=cssName%> .padding-bottom-10{padding-bottom: 10px}
.warp<%=cssName%> .padding-left-0{ padding-left: 0px;}
.warp<%=cssName%> .padding-left-2{padding-left: 2px}
.warp<%=cssName%> .container{max-width:1200px;}
.warp<%=cssName%> a{color:#333;text-decoration:none;}
.warp<%=cssName%> a:hover{color:#0062d0}
.warp<%=cssName%> a:focus{color:#0062d0}
.warp<%=cssName%> a.btn-more{transition:all 0.3s ease;-moz-transition:all 0.3s ease; /* Firefox 4 */-webkit-transition:all 0.3s ease; /* Safari and Chrome */-o-transition:all 0.3s ease; /* Opera */}
.warp<%=cssName%> .card1{white-space: nowrap;text-overflow: ellipsis;overflow: hidden;display: block;}
.warp<%=cssName%> .images-list .row .item a{display:block;overflow:hidden;}
.warp<%=cssName%> .images-list .row .item img{display:block;width:100%;height:auto;transition: all 0.3s ease-out 0s;}
.warp<%=cssName%> .images-list .row .item a:hover img{transform:scale(1.08,1.08);}
.warp<%=cssName%> .nav-fenye{height:auto;width:100%;margin-top:30px;}
.warp<%=cssName%> .nav-content{width:100%;height:100%; overflow-wrap: break-word;padding: 0px;border-color: #0d62be;border-width: 2px;background-color: transparent;border-style: solid;}
.warp<%=cssName%> .nav-header{width:100%;height:auto;background-color: #0d62be;}
.warp<%=cssName%> .header-wenzi{padding-top: 20px;width:100%;display: block; font-family: "Microsoft YaHei";font-size: 30px;color: rgb(255, 255, 255);text-align:center;}
.warp<%=cssName%> .header-wenzi1{padding-bottom: 20px;font-family: "Microsoft YaHei";font-size: 22px;color: rgb(255, 255, 255);width:100%;display: block;text-align: center; }
.warp<%=cssName%> .header-top{left: 8px;top: 17px;position: absolute;width: 34px;height: 34px;transform: rotate(31deg);transform-origin: 0px 0px;}
.warp<%=cssName%> .top-content{margin: 15px 0; border: none;border-top-color: currentcolor;border-top-style: none;border-top-width: medium;border-top: 2px solid #ffffff;height: 0;}
.warp<%=cssName%> .footer-content{margin: 15px 0; border: none;border-top-color: currentcolor;border-top-style: none;border-top-width: medium;border-top: 2px solid #ffffff;height: 0;}
.warp<%=cssName%> .header-footer{right: -11px;top: 117px;position: absolute;z-index: 137;transform: rotate(31deg);transform-origin: 0px 0px;padding: 0px;border-color: transparent;border-width: 0px;overflow: hidden;width: 34px;height: 34px;}
.warp<%=cssName%> .nav-footer{padding: 10px;clear: both;overflow: hidden;overflow-wrap: break-word;border-color: transparent;border-width: 0px;height: auto;background-color: #0d62be;width: 100%;border-style: solid;}
.warp<%=cssName%> .footer-img{ overflow: hidden;height: auto;border-width: 0px;border-style: solid;padding: 6px 10px 0px;}
.warp<%=cssName%> .footer-img img{width:42px;height:auto;}
.warp<%=cssName%> .footer-title{font-size: 14px;font-family: "Microsoft YaHei";color: rgb(255, 255, 255);}
.warp<%=cssName%> .footer-wenzi{float:left;}
.warp<%=cssName%> .footer-wenzi strong{font-size: 20px;font-family: "Microsoft YaHei";color: #fff;}
.warp<%=cssName%> .footer-wenzi span{overflow: hidden;}
.warp<%=cssName%> .nav-message{z-index: 101;width: 80%;margin:auto;padding-top:20px;}
.warp<%=cssName%> .nav-message ul{width:100%;}
.warp<%=cssName%> .nav-message ul li{width:100%;margin-bottom:20px;padding:10px 0;transition:all .7s ;background:url(<%=resUrl%>images/title1.png)no-repeat 20% 9px  #0d62be;padding-left:30%;}
.warp<%=cssName%> .nav-message ul li a{color:#fff;width:100%;display:block;font-size: 16px;padding-left:3%;}
.warp<%=cssName%> .nav-message ul li:hover{background-color: #326ec0;}
.warp<%=cssName%> .nav-box{clear:both;text-align:left;margin-bottom:20px;width:100%;overflow:visible;}
.warp<%=cssName%> .nav-box li{width:100%;padding:0px;text-align:left;position:relative;display:block;}
.warp<%=cssName%> .nav-box li a{display:block;text-align:center;line-height:50px;color:#fff;font-family:"Microsoft YaHei",Tahoma,Verdana,"Simsun";font-size:16px;background-color: #0d62be;margin: 20px 20px;}
.warp<%=cssName%> .nav-box li a:hover,.warp<%=cssName%> .nav-box li.current > a{background-color: #326ec0;}
@media (min-width:1000px){
.warp<%=cssName%> .images-x li{margin-bottom:30px;}
.warp<%=cssName%> .images-x li .box{padding: 15px;}
.warp<%=cssName%> .wordsmax{margin:40px 0 20px;}
.warp<%=cssName%> .wordsmax:after{width:12%;}
.warp<%=cssName%> .images-list li{padding-left:5px;padding-right:5px;}}
@media (max-width:1000px){
.warp<%=cssName%> .images-x li{margin-bottom:10px;padding-left:5px;padding-right:5px;}
.warp<%=cssName%> .images-x li .box{padding: 6px;}
.warp<%=cssName%> .images-x li .box .title{font-size:13px;}
.warp<%=cssName%> .wordsmax{margin:-24px 0 30px;}
.warp<%=cssName%> .wordsmax:after{width:30%;}
.warp<%=cssName%> .wordsmax .fontsize{font-size:18px!important;}
.warp<%=cssName%> .wordsmax .text{font-size:14px!important;}
.warp<%=cssName%> .footer-wenzi span{font-size: 13px;}}
.warp<%=cssName%> .wordsmax{position: relative;left: 0;z-index: 1;top: 0;width:100%;border-bottom:1px solid #d9d9d9;}
.warp<%=cssName%> .wordsmax .fontsize{border-left:2px solid #0d62be;padding-left:10px;}
.warp<%=cssName%> .wordsmax:after{content:"";height:3px;background-color:#0d62be;display:block;}
.warp<%=cssName%> .images-x li .box{border: 1px solid #d9d9d9;}
.warp<%=cssName%> .images-x li .box:hover{border-color:#0d62be;}
.warp<%=cssName%> .images-x li .box .box-img{width:42px;height:42px;position:absolute;top: 40%;left: 40%;opacity:0;transition:all .7s ;transform:scale(0.0);}
.warp<%=cssName%> .images-x li .box .box-img img{width:100%!important;height:100%!important;}
.warp<%=cssName%> .images-x li .box .box-mask{top: 0;left: 0;background: rgba(43,65,164,0.3)none repeat scroll 0% 0%;width: 100%;height: 100%;transform:scale(0.0);display:block;position:absolute;opacity:0;transition:all .7s ;}
.warp<%=cssName%> .images-list .row{text-align:center;}
.warp<%=cssName%> .images-list .row .item a:hover .box-img{opacity:1;transform:scale(1.1)}
.warp<%=cssName%> .images-list .row .item a:hover img{transform: scale(1.08,1.08);}
.warp<%=cssName%> .images-list .row .item a:hover .box-mask{opacity:1;transform:scale(1.1)}
.warp<%=cssName%> .images-list .row .item a{position:relative;}
@media (min-width:1000px){
.warp<%=cssName%> .home-pro{padding-bottom: 50px;}
.warp<%=cssName%> .home-pro .images-list li{padding-left: 5px;padding-right: 5px;margin-bottom: 10px;}
.warp<%=cssName%> .home-pro .nav-box{margin-bottom:20px;/*margin-bottom: 118px;*/}} 
@media (max-width:1000px){
.warp<%=cssName%> .home-pro{padding-bottom: 30px;}
.warp<%=cssName%> .home-pro .nav-message{width:100%;}
.warp<%=cssName%> .home-pro .header-wenzi{font-size: 22px;}
.warp<%=cssName%> .home-pro .header-wenzi1{font-size: 20px;}
.warp<%=cssName%> .home-pro .nav-fenye{margin-top: 10px;}
.warp<%=cssName%> .home-pro .nav-message ul li{width: 47%;float: left;background: #0d62be;padding: 4px;margin: 0 4px 16px;text-align: center;}
.warp<%=cssName%> .home-pro .header-footer{top: 80px;}
.warp<%=cssName%> .home-pro .nav-message ul li a{font-size:14px;}
.warp<%=cssName%> .home-pro .header-top{top: 0px;}}
.warp<%=cssName%> .home-pro .wordsmax .promore{position: absolute;right: 0;top: 16px;}
.warp<%=cssName%> .home-pro .wordsmax .promore a{padding: 10px 20px;background-color: #0d62be;color: #fff;}
.warp<%=cssName%> .home-pro .wordsmax .promore a:hover{background-color:#326ec0;}

</style>

    <section class="warp<%=cssName%>">
        <!--产品中心-->
        <div class="container home-pro">
            <div class="row">
                <div class="col-md-3 padding-top-20">
                    <div class="container position-relative visible-md hidden-sm hidden-xs">
                        <div class="nav-fenye">
                            <div class="nav-content">
                                <div class="nav-header">
                                    <span class="header-wenzi text-uppercase">PRODUCTS</span>
                                    <div class="header-top">
                                        <div class="top-content"></div>
                                    </div>
                                    <span class="header-wenzi1">产品中心</span>
                                    <div class="header-footer">
                                        <div class="footer-content"></div>
                                    </div>
                                </div>
                                <div class="nav-box" id="subNavs">
                                    <ul> 
<%rs.open "select * from " & db_PREFIX & "webcolumn where  isthrough=1 AND parentid in("& getNameToAllId( "产品中心") &") order by sortRank desc,id desc" ,conn,1,1
for i=1 to 12
if rs.eof then exit for
%>
                        <li>
                            <a href="<%=getNavUrl(rs("id"),rs("columnType"))%>"><%=uTitle & rs("columnName")%>
                            </a>
                        </li> 
<%rs.movenext:next:rs.close%>
                                    </ul>
                                </div>
                                <div class="nav-footer ">
                                    <div class="footer-img col-md-3 col-sm-3 col-xs-3"><img src="<%=resUrl%>images/t7.png">
                                    </div>
                                    <div class="footer-wenzi col-md-9 col-sm-9 col-xs-9 padding-left-0">
                                        <span class="footer-title">全国服务热线：</span><br>
                                        <strong><%=webphone%></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="container position-relative hidden-md hidden-sm visible-xs">
                        <div class="nav-fenye">
                            <div class="nav-content">
                                <div class="nav-header">
                                    <span class="header-wenzi text-uppercase">PRODUCTS</span>
                                    <div class="header-top">
                                        <div class="top-content"></div>
                                    </div>
                                    <span class="header-wenzi1">产品中心</span>
                                    <div class="header-footer">
                                        <div class="footer-content"></div>
                                    </div>
                                </div>
                                <div class="nav-message" id="subNavs">
                                    <ul> 
<%rs.open "select * from " & db_PREFIX & "webcolumn where  isthrough=1 AND parentid in("& getNameToAllId( "产品中心") &") order by sortRank desc,id desc" ,conn,1,1
for i=1 to 12
if rs.eof then exit for
%>
                        <li>
                            <a href="<%=getNavUrl(rs("id"),rs("columnType"))%>"><%=uTitle & rs("columnName")%>
                            </a>
                        </li> 
<%rs.movenext:next:rs.close%>
                                    </ul>
                                </div>
                                <div class="nav-footer ">
                                    <div class="footer-img col-md-3 col-sm-3 col-xs-3"><img src="<%=resUrl%>images/t7.png">
                                    </div>
                                    <div class="footer-wenzi col-md-9 col-sm-9 col-xs-9 padding-left-0">
                                        <span class="footer-title">全国服务热线：</span><br>
                                        <strong><%=webphone%></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-9 padding-top-20">
                    <div class="wordsmax visible-md hidden-sm hidden-xs">
                        <p class="padding-bottom-10">
                            <span class="fontsize font-size-22">产品中心</span>/
                            <span class="font-size-16 text-gray text-uppercase text">product center</span>
                        </p>
                        <div class="promore">
                            <a href="<%=getNavNameToUrl("","产品中心")%>" class="btn-more">MORE+</a>
                        </div>
                    </div>
                    <div class="images-list imgae-same-size" id="productList">
                        <ul class="row images-x">



<%rs.open "select * from " & db_PREFIX & "ArticleDetail where isthrough=1 AND parentId in("& getNameToAllId( "产品中心") &") order by sortRank asc,id desc" ,conn,1,1
for i=1 to 12
if rs.eof then exit for
%>  

                            <li class="col-md-3 col-sm-3 col-xs-6 item">
                                <div class="box">
                                    <a href="<%=getArticleUrl(rs("id"))%>"><img src="<%=rs("smallimage")%>">
                                        <div class="box-mask"></div>
                               <div class="box-img"><img src="<%=resUrl%>images/img-content.png" style="width: 176px; height: 191px;">
                                        </div>
                                    </a>
                                    <a href="<%=getArticleUrl(rs("id"))%>" class="text-center line-height-2 card1 padding-left-2 padding-right-2 title"><%=uTitle&rs("title")%></a>
                                </div>
                            </li>

<%rs.movenext:next:rs.close%>
 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
