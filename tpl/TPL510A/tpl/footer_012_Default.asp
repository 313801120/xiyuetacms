﻿<% 
'#禁止自动更新当前文件'  #号去掉代表此文件不被程序自动更新替换掉

                 '''
'               (0 0)
'   +-----oOO----(_)------------+
'   |                           |
'   |    author:XIYUETA         |
'   |    QQ:313801120           |
'   |    www.xiyueta.com        |
'   |                           |
'   +------------------oOO------+
'              |__|__|
'               || ||
'              ooO Ooo

cssName="footer012" 
 %>
<style>

.wrap<%=cssName%> *{
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    transition:none 0.4s linear;
    -webkit-transition:none 0.4s linear;
    -moz-transition:none 0.4s linear;
}
.wrap<%=cssName%> *,.wrap<%=cssName%> ::after,.wrap<%=cssName%> ::before{box-sizing: border-box;}
.wrap<%=cssName%> html{
    font-size: 10px; /*10 ÷ 16 × 100% = 62.5%*/
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
}
.wrap<%=cssName%> body{
    font-size: 1.4rem;
    font-size: 14px;
    margin: 0 auto;
    font-family: "Microsoft Yahei", "Helvetica Neue", Helvetica, Arial, sans-serif;
    -ms-behavior: url(images/backgroundsize.min.jpg);
    behavior: url(images/backgroundsize.min.jpg);
    line-height: 1.5;
    color: #444;
}
.wrap<%=cssName%> div,.wrap<%=cssName%> span,.wrap<%=cssName%> applet,.wrap<%=cssName%> object,.wrap<%=cssName%> iframe,.wrap<%=cssName%> h1,.wrap<%=cssName%> h2,.wrap<%=cssName%> h3,.wrap<%=cssName%> h4,.wrap<%=cssName%> h5,.wrap<%=cssName%> h6,.wrap<%=cssName%> p,.wrap<%=cssName%> blockquote,.wrap<%=cssName%> pre,.wrap<%=cssName%> a,.wrap<%=cssName%> abbr,.wrap<%=cssName%> acronym,.wrap<%=cssName%> address,.wrap<%=cssName%> big,.wrap<%=cssName%> cite,.wrap<%=cssName%> code,.wrap<%=cssName%> del,.wrap<%=cssName%> dfn,.wrap<%=cssName%> em,.wrap<%=cssName%> img,.wrap<%=cssName%> ins,.wrap<%=cssName%> kbd,.wrap<%=cssName%> q,.wrap<%=cssName%> s,.wrap<%=cssName%> samp,.wrap<%=cssName%> small,.wrap<%=cssName%> strike,.wrap<%=cssName%> strong,.wrap<%=cssName%> sub,.wrap<%=cssName%> sup,.wrap<%=cssName%> tt,.wrap<%=cssName%> var,.wrap<%=cssName%> b,.wrap<%=cssName%> u,.wrap<%=cssName%> i,.wrap<%=cssName%> center,.wrap<%=cssName%> dl,.wrap<%=cssName%> dt,.wrap<%=cssName%> dd,.wrap<%=cssName%> ol,.wrap<%=cssName%> ul,.wrap<%=cssName%> li,.wrap<%=cssName%> fieldset,.wrap<%=cssName%> form,.wrap<%=cssName%> label,.wrap<%=cssName%> legend,.wrap<%=cssName%> table,.wrap<%=cssName%> caption,.wrap<%=cssName%> tbody,.wrap<%=cssName%> tfoot,.wrap<%=cssName%> thead,.wrap<%=cssName%> tr,.wrap<%=cssName%> th,.wrap<%=cssName%> td,.wrap<%=cssName%> article,.wrap<%=cssName%> aside,.wrap<%=cssName%> canvas,.wrap<%=cssName%> details,.wrap<%=cssName%> embed,.wrap<%=cssName%> figure,.wrap<%=cssName%> figcaption,.wrap<%=cssName%> footer,.wrap<%=cssName%> header,.wrap<%=cssName%> hgroup,.wrap<%=cssName%> menu,.wrap<%=cssName%> nav,.wrap<%=cssName%> output,.wrap<%=cssName%> ruby,.wrap<%=cssName%> section,.wrap<%=cssName%> summary,.wrap<%=cssName%> time,.wrap<%=cssName%> mark,.wrap<%=cssName%> audio,.wrap<%=cssName%> video{
    margin:0; padding:0; border:0;outline:0;font-family: inherit;font-size: inherit;line-height:inherit;color:inherit;vertical-align:baseline;
}
.wrap<%=cssName%> article,.wrap<%=cssName%> aside,.wrap<%=cssName%> details,.wrap<%=cssName%> figcaption,.wrap<%=cssName%> figure,.wrap<%=cssName%> footer,.wrap<%=cssName%> header,.wrap<%=cssName%> hgroup,.wrap<%=cssName%> main,.wrap<%=cssName%> menu,.wrap<%=cssName%> nav,.wrap<%=cssName%> section,.wrap<%=cssName%> summary{display: block;}
.wrap<%=cssName%> li{list-style:none;}
.wrap<%=cssName%> img{max-width: 100%;vertical-align:middle;}
.wrap<%=cssName%> a{color:inherit;text-decoration: none;}
.wrap<%=cssName%> a:active,.wrap<%=cssName%> a:hover{outline: 0;}
.wrap<%=cssName%> a:hover,.wrap<%=cssName%> a:focus{text-decoration: none;color:inherit}
.wrap<%=cssName%> img{border:0;}
.wrap<%=cssName%> .text-white{color: #fff;}
.wrap<%=cssName%> .text-graylight{color: #cccccc;}
.wrap<%=cssName%> .clearfix:before,.wrap<%=cssName%> .clearfix:after,.wrap<%=cssName%> .dl-horizontal dd:before,.wrap<%=cssName%> .dl-horizontal dd:after,.wrap<%=cssName%> .container:before,.wrap<%=cssName%> .container:after,.wrap<%=cssName%> .container-fluid:before,.wrap<%=cssName%> .container-fluid:after,.wrap<%=cssName%> .row:before,.wrap<%=cssName%> .row:after,.wrap<%=cssName%> .form-horizontal .form-group:before,.wrap<%=cssName%> .form-horizontal .form-group:after,.wrap<%=cssName%> .btn-toolbar:before,.wrap<%=cssName%> .btn-toolbar:after,.wrap<%=cssName%> .btn-group-vertical > .btn-group:before,.wrap<%=cssName%> .btn-group-vertical > .btn-group:after,.wrap<%=cssName%> .nav:before,.wrap<%=cssName%> .nav:after,.wrap<%=cssName%> .navbar:before,.wrap<%=cssName%> .navbar:after,.wrap<%=cssName%> .navbar-header:before,.wrap<%=cssName%> .navbar-header:after,.wrap<%=cssName%> .navbar-collapse:before,.wrap<%=cssName%> .navbar-collapse:after,.wrap<%=cssName%> .pager:before,.wrap<%=cssName%> .pager:after,.wrap<%=cssName%> .panel-body:before,.wrap<%=cssName%> .panel-body:after,.wrap<%=cssName%> .modal-header:before,.wrap<%=cssName%> .modal-header:after,.wrap<%=cssName%> .modal-footer:before,.wrap<%=cssName%> .modal-footer:after{content: " ";display: table;}
.wrap<%=cssName%> .clearfix:after,.wrap<%=cssName%> .dl-horizontal dd:after,.wrap<%=cssName%> .container:after,.wrap<%=cssName%> .container-fluid:after,.wrap<%=cssName%> .row:after,.wrap<%=cssName%> .form-horizontal .form-group:after,.wrap<%=cssName%> .btn-toolbar:after,.wrap<%=cssName%> .btn-group-vertical > .btn-group:after,.wrap<%=cssName%> .nav:after,.wrap<%=cssName%> .navbar:after,.wrap<%=cssName%> .navbar-header:after,.wrap<%=cssName%> .navbar-collapse:after,.wrap<%=cssName%> .pager:after,.wrap<%=cssName%> .panel-body:after,.wrap<%=cssName%> .modal-header:after,.wrap<%=cssName%> .modal-footer:after{clear: both;}
.wrap<%=cssName%> .visible-xs,.wrap<%=cssName%> .visible-sm,.wrap<%=cssName%> .visible-md,.wrap<%=cssName%> .visible-lg{display: none !important;}
.wrap<%=cssName%> .text-center{text-align:center}
.wrap<%=cssName%> .text-left{text-align:left}
.wrap<%=cssName%> .text-right{text-align:right}
.wrap<%=cssName%> .container{margin-right: auto;margin-left: auto;padding-left: 15px;padding-right: 15px;}
@media (min-width:768px){
.wrap<%=cssName%> .container{width: 100%;}}
@media (min-width:992px){
.wrap<%=cssName%> .container{width:100%;}}
.wrap<%=cssName%> .row{margin-left: -15px;margin-right: -15px;}
.wrap<%=cssName%> .row .row{margin-left:0px;margin-right:0px}
.wrap<%=cssName%> .col-xs-1,.wrap<%=cssName%> .col-sm-1,.wrap<%=cssName%> .col-md-1,.wrap<%=cssName%> .col-lg-1,.wrap<%=cssName%> .col-xs-2,.wrap<%=cssName%> .col-sm-2,.wrap<%=cssName%> .col-md-2,.wrap<%=cssName%> .col-lg-2,.wrap<%=cssName%> .col-xs-3,.wrap<%=cssName%> .col-sm-3,.wrap<%=cssName%> .col-md-3,.wrap<%=cssName%> .col-lg-3,.wrap<%=cssName%> .col-xs-4,.wrap<%=cssName%> .col-sm-4,.wrap<%=cssName%> .col-md-4,.wrap<%=cssName%> .col-lg-4,.wrap<%=cssName%> .col-xs-5,.wrap<%=cssName%> .col-sm-5,.wrap<%=cssName%> .col-md-5,.wrap<%=cssName%> .col-lg-5,.wrap<%=cssName%> .col-xs-6,.wrap<%=cssName%> .col-sm-6,.wrap<%=cssName%> .col-md-6,.wrap<%=cssName%> .col-lg-6,.wrap<%=cssName%> .col-xs-7,.wrap<%=cssName%> .col-sm-7,.wrap<%=cssName%> .col-md-7,.wrap<%=cssName%> .col-lg-7,.wrap<%=cssName%> .col-xs-8,.wrap<%=cssName%> .col-sm-8,.wrap<%=cssName%> .col-md-8,.wrap<%=cssName%> .col-lg-8,.wrap<%=cssName%> .col-xs-9,.wrap<%=cssName%> .col-sm-9,.wrap<%=cssName%> .col-md-9,.wrap<%=cssName%> .col-lg-9,.wrap<%=cssName%> .col-xs-10,.wrap<%=cssName%> .col-sm-10,.wrap<%=cssName%> .col-md-10,.wrap<%=cssName%> .col-lg-10,.wrap<%=cssName%> .col-xs-11,.wrap<%=cssName%> .col-sm-11,.wrap<%=cssName%> .col-md-11,.wrap<%=cssName%> .col-lg-11,.wrap<%=cssName%> .col-xs-12,.wrap<%=cssName%> .col-sm-12,.wrap<%=cssName%> .col-md-12,.wrap<%=cssName%> .col-lg-12,.wrap<%=cssName%> .col-xs-avg-5,.wrap<%=cssName%> .col-xs-avg-4,.wrap<%=cssName%> .col-xs-avg-3,.wrap<%=cssName%> .col-xs-avg-2,.wrap<%=cssName%> .col-xs-avg-1,.wrap<%=cssName%> .col-sm-avg-5,.wrap<%=cssName%> .col-sm-avg-4,.wrap<%=cssName%> .col-sm-avg-3,.wrap<%=cssName%> .col-sm-avg-2,.wrap<%=cssName%> .col-sm-avg-1,.wrap<%=cssName%> .col-md-avg-5,.wrap<%=cssName%> .col-md-avg-4,.wrap<%=cssName%> .col-md-avg-3,.wrap<%=cssName%> .col-md-avg-2,.wrap<%=cssName%> .col-md-avg-1,.wrap<%=cssName%> .col-lg-avg-5,.wrap<%=cssName%> .col-lg-avg-4,.wrap<%=cssName%> .col-lg-avg-3,.wrap<%=cssName%> .col-lg-avg-2,.wrap<%=cssName%> .col-lg-avg-1{
    position: relative;
    min-height: 1px;
    padding-left: 15px;
    padding-right: 15px;
}
.wrap<%=cssName%> .col-xs-1,.wrap<%=cssName%> .col-xs-2,.wrap<%=cssName%> .col-xs-3,.wrap<%=cssName%> .col-xs-4,.wrap<%=cssName%> .col-xs-5,.wrap<%=cssName%> .col-xs-6,.wrap<%=cssName%> .col-xs-7,.wrap<%=cssName%> .col-xs-8,.wrap<%=cssName%> .col-xs-9,.wrap<%=cssName%> .col-xs-10,.wrap<%=cssName%> .col-xs-11,.wrap<%=cssName%> .col-xs-12,.wrap<%=cssName%> .col-xs-avg-5,.wrap<%=cssName%> .col-xs-avg-4,.wrap<%=cssName%> .col-xs-avg-3,.wrap<%=cssName%> .col-xs-avg-2,.wrap<%=cssName%> .col-xs-avg-1{
    float: left;
}
.wrap<%=cssName%> .col-xs-6{width: 50%;}
@media (max-width: 767px){
.wrap<%=cssName%> .hidden-xs{display: none !important;}}
@media (min-width: 768px){
.wrap<%=cssName%> .col-sm-1,.wrap<%=cssName%> .col-sm-2,.wrap<%=cssName%> .col-sm-3,.wrap<%=cssName%> .col-sm-4,.wrap<%=cssName%> .col-sm-5,.wrap<%=cssName%> .col-sm-6,.wrap<%=cssName%> .col-sm-7,.wrap<%=cssName%> .col-sm-8,.wrap<%=cssName%> .col-sm-9,.wrap<%=cssName%> .col-sm-10,.wrap<%=cssName%> .col-sm-11,.wrap<%=cssName%> .col-sm-12,.wrap<%=cssName%> .col-sm-avg-5,.wrap<%=cssName%> .col-sm-avg-4,.wrap<%=cssName%> .col-sm-avg-3,.wrap<%=cssName%> .col-sm-avg-2,.wrap<%=cssName%> .col-sm-avg-1{
        float: left;
    }
.wrap<%=cssName%> .col-sm-6{width: 50%;}
.wrap<%=cssName%> .hidden-sm{display: none !important;}
.wrap<%=cssName%> .visible-sm{display: block !important;}}
@media (min-width: 992px){
.wrap<%=cssName%> .col-md-1,.wrap<%=cssName%> .col-md-2,.wrap<%=cssName%> .col-md-3,.wrap<%=cssName%> .col-md-4,.wrap<%=cssName%> .col-md-5,.wrap<%=cssName%> .col-md-6,.wrap<%=cssName%> .col-md-7,.wrap<%=cssName%> .col-md-8,.wrap<%=cssName%> .col-md-9,.wrap<%=cssName%> .col-md-10,.wrap<%=cssName%> .col-md-11,.wrap<%=cssName%> .col-md-12,.wrap<%=cssName%> .col-md-avg-5,.wrap<%=cssName%> .col-md-avg-4,.wrap<%=cssName%> .col-md-avg-3,.wrap<%=cssName%> .col-md-avg-2,.wrap<%=cssName%> .col-md-avg-1{
        float: left;
    }
.wrap<%=cssName%> .col-md-6{width: 50%;}
.wrap<%=cssName%> .col-md-4{width: 33.33333333%;}
.wrap<%=cssName%> .visible-md{display: block !important;}}

.wrap<%=cssName%> button,.wrap<%=cssName%> html input[type="button"],.wrap<%=cssName%> input[type="reset"],.wrap<%=cssName%> input[type="submit"]{-webkit-appearance: button;cursor: pointer;}
.wrap<%=cssName%> button[disabled],.wrap<%=cssName%> html input[disabled]{cursor: default;}
.wrap<%=cssName%> .font-weight-700{font-weight:700}
.wrap<%=cssName%> .line-height-2{ line-height:2;}
.wrap<%=cssName%> .font-size-12{font-size: 12px}
.wrap<%=cssName%> .font-size-18{font-size: 18px}
.wrap<%=cssName%> .padding-top-16{padding-top: 16px}
.wrap<%=cssName%> .padding-top-20{padding-top: 20px}
.wrap<%=cssName%> .padding-right-0{ padding-right: 0px;}
.wrap<%=cssName%> .padding-right-6{padding-right: 6px}
.wrap<%=cssName%> .padding-right-10{padding-right: 10px}
.wrap<%=cssName%> .padding-bottom-16{padding-bottom: 16px}
.wrap<%=cssName%> .padding-left-0{ padding-left: 0px;}
.wrap<%=cssName%> .padding-left-10{padding-left: 10px}
.wrap<%=cssName%> .margin-top-40{margin-top: 40px}
.wrap<%=cssName%> .container{max-width:1200px;}
.wrap<%=cssName%> a{color:#333;text-decoration:none;}
.wrap<%=cssName%> a:hover{color:#0062d0}
.wrap<%=cssName%> a:focus{color:#0062d0}
.wrap<%=cssName%> .bg-black{background-color: #326ec0;padding-top:34px;}
.wrap<%=cssName%> footer{width:100%;min-height:95px;line-height:2.5;}
.wrap<%=cssName%> .ulli ul li{line-height: 1.9;}
.wrap<%=cssName%> .ulli ul li:not(:nth-child(-n+7)){display:none;}
.wrap<%=cssName%> .ulli ul li a{color: #fff;font-size: 15px;}
.wrap<%=cssName%> .ulli ul li a:hover{color:#FFFFFF;}
.wrap<%=cssName%> .foottitle span i{display: block;border-bottom: 2px solid #fff;width: 30px;margin-bottom: 10px;}
.wrap<%=cssName%> .contact li:not(:first-child){line-height:2;font-size:15px;}
.wrap<%=cssName%> .copyright-bg{background-color: #0d62be;}
.wrap<%=cssName%> .copyright-bg .box a,.wrap<%=cssName%> .copyright-bg .box{color:#FFF;font-size:13px;}
.wrap<%=cssName%> .copyright-bg .box a:hover{text-decoration:underline;}
.wrap<%=cssName%> .copyright a:hover{text-decoration:underline;color:#fff;}
@media (max-width:1000px){
footer{/*.wrap<%=cssName%> margin-bottom:50px;*/}}


</style> 
    <footer class="wrap<%=cssName%>">
        <div class="bg-black">
            <div class="container">
                <ul class="row">
                    <li>
                        <div class="col-md-4 visible-md hidden-sm hidden-xs foottitle">
                            <div class="row padding-left-0 padding-right-0">


<%
dim nNavCount012:nNavCount012=0
    rs.open "select * from " & db_PREFIX & "WebColumn where parentId=-1 and isThrough=1 and flags like'%foot%' order by sortRank asc" ,conn,1,1
for i=1 to rs.recordcount
    if rs.eof or nNavCount012>=2 then exit for
if rs.eof then exit for
    rsx.open "select * from " & db_PREFIX & "WebColumn where parentId="& rs("id") &" and isThrough=1 order by sortRank asc" ,conn,1,1
    if not rsx.eof then
        nNavCount012=nNavCount012+1
%> 


<div class="col-md-6 text-center padding-left-0 padding-right-0 text-left">
                                    <span class="text-white font-size-18 font-weight-700"><%=IIF(rs("columnType")<>"home",uTitle,"") & rs("columnname")%>
                                        <i></i>
                                    </span>
                                    <span class="text-grayLight ulli">
                                        <ul>
        <%

        ' while not rsx.eof


        for j=1 to rsx.recordcount
            if j=5 then

    %>


 
            <li>
                                <a href="<%=getNavUrl(rs("id"),rs("columnType"))%>">查看更多+</a>
                            </li>

                            <%
exit for
                            else%>

  <li>
                                                <a href="<%=getNavUrl(rsx("id"),rsx("columnType"))%>"><%=rsx("columnname")%></a>
                                            </li>
 
                            <%end if%>



        <%rsx.movenext:next%>
                                        </ul>
                                    </span>
                                </div>


<%
                end if:rsx.close%>
<%rs.movenext:next:rs.close%> 
 
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 contact foottitle">
                            <ul class="text-white">
                                <li>
                                    <span class="font-size-18 font-weight-700">联系我们
                                        <i></i>
                                    </span>
                                </li>
                                <li>手机：<%=webphone%>
                                    <span>电话：<%=webtel%></span>
                                </li>
                                <li>传真：<%=webfax%>
                                    <span>网址：<%=weburl%></span>
                                </li>
                                <li>邮箱：<%=webemail%>
                                    <span>QQ:1483771556</span>
                                </li>
                                <li>地址：<%=webaddress%></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-6 edge weixin text-white visible-md visible-sm hidden-xs">
                            <div class="row number imgae-same-size text-center">
                                <div class="col-md-6 col-sm-6 col-xs-6 padding-top-20 text-right"><img src="<%=webqrcode%>" data-bindresize="1" style="width: 98px; height: 98px;">
                                    <p class="font-size-12 padding-right-6">扫码关注公众号</p>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 padding-top-20"><img src="<%=webqrcode%>" style="width: 98px; height: 98px;">
                                    <p class="font-size-12 text-center">微信公众号</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="copyright-bg padding-top-16 padding-bottom-16 line-height-2 margin-top-40 padding-left-10 padding-right-10">
                <div class="box text-center"><%=webcopyright%>
                </div>
                <div class="copyright text-center">
                    <a href="http://xiyueta.com/" target="_blank" class="text-white">Powered By xiyeuta Cms</a>
                </div>
            </div>
        </div>
    </footer>
